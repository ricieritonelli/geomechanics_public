#ifndef CONSTANT_FIELD_CREATOR_H
#define CONSTANT_FIELD_CREATOR_H

#include "../PropertyFieldCreator.h"

// CLASS DEFINITION ==============================================================================================
class ConstantFieldCreator : public PropertyFieldCreator
{
protected:

	// Data Members ----------------------------------------------------------------------------------------------
	double propertyValue;
	
public:

	// Constructor and Destructor --------------------------------------------------------------------------------
	ConstantFieldCreator( GridPtr grid, double propertyValue );
	virtual ~ConstantFieldCreator(){}

	// Class Interface -------------------------------------------------------------------------------------------
	VectorPtr createPropertyFieldOnCells();
	VectorPtr createPropertyFieldOnFaces();
	VectorPtr createPropertyFieldOnVertices();

}; // class ConstantInterpolator

typedef SharedPointer< ConstantFieldCreator > ConstantFieldCreatorPtr;

#endif