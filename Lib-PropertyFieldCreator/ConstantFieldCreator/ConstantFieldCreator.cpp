#include "ConstantFieldCreator.h"

// Constructor ---------------------------------------------------------------------------------------------------
ConstantFieldCreator::ConstantFieldCreator( GridPtr grid, double propertyValue ): PropertyFieldCreator( grid ), propertyValue( propertyValue ){}


// Class Interface -----------------------------------------------------------------------------------------------
VectorPtr ConstantFieldCreator::createPropertyFieldOnCells()
{
	VectorPtr field( new Vector( ( this->grid->getCells() ).size(), this->propertyValue ) );
	return field;
}

VectorPtr ConstantFieldCreator::createPropertyFieldOnFaces()
{
	VectorPtr field( new Vector( this->grid->getNumberOfFaces(), this->propertyValue ) );
	return field;
}

VectorPtr ConstantFieldCreator::createPropertyFieldOnVertices()
{
	VectorPtr field( new Vector( ( this->grid->getVertices() ).size(), this->propertyValue ) );
	return field;
}