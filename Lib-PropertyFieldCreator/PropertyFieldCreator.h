#ifndef PROPERTY_FIELD_CREATOR_H
#define PROPERTY_FIELD_CREATOR_H

#include "Config.h"

// CLASS DEFINITION ==============================================================================================
class PropertyFieldCreator
{
protected:

	// Data Members ----------------------------------------------------------------------------------------------
	GridPtr grid;

public:

	// Constructor and Destructor --------------------------------------------------------------------------------
	PropertyFieldCreator( GridPtr grid ) : grid( grid ){}
	virtual ~PropertyFieldCreator(){}

	// Class Interface -------------------------------------------------------------------------------------------
	virtual VectorPtr createPropertyFieldOnFaces() = 0;
	virtual VectorPtr createPropertyFieldOnCells() = 0;
	virtual VectorPtr createPropertyFieldOnVertices() = 0;

}; // class PropertyFieldCreator

typedef SharedPointer< PropertyFieldCreator > PropertyFieldCreatorPtr;

#endif