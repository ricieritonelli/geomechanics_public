#ifndef HORIZONTALLY_DIVIDED_FIELD_CREATOR_H
#define HORIZONTALLY_DIVIDED_FIELD_CREATOR_H

#include "../PropertyFieldCreator.h"

// CLASS DEFINITION ==============================================================================================
class HorizontallyDividedFieldCreator : public PropertyFieldCreator
{
protected:

	// Data Members ----------------------------------------------------------------------------------------------
	Vector lowerYValues;
	Vector upperYValues;
	Vector propertyValues;
	
public:

	// Constructor and Destructor --------------------------------------------------------------------------------
	HorizontallyDividedFieldCreator( GridPtr grid );
	virtual ~HorizontallyDividedFieldCreator(){}

	// Class Interface -------------------------------------------------------------------------------------------
	void appendFieldSector( double lowerYValue, double upperYValue, double propertyValue );
	VectorPtr createPropertyFieldOnCells();
	VectorPtr createPropertyFieldOnFaces();
	VectorPtr createPropertyFieldOnVertices();

private:

	// Private Member Functions ----------------------------------------------------------------------------------
	void simpleSectorLocationChecker( bool& positionFound, double& yPosition, VectorPtr field );
	void proximityWithTheDivisionsBetweenSectorsChecker( bool& positionFound, double& yPosition, double& yPositionNorth, double& yPositionSouth, VectorPtr field );
	void coincidingPositionWithTheDivisionsBetweenSectorsChecker( bool& positionFound, double& yPosition, VectorPtr field );

}; // class HorizontallyDividedFieldCreator

typedef SharedPointer< HorizontallyDividedFieldCreator > HorizontallyDividedFieldCreatorPtr;

#endif