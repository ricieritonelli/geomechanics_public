#include "HorizontallyDividedFieldCreator.h"

// Constructor ---------------------------------------------------------------------------------------------------
HorizontallyDividedFieldCreator::HorizontallyDividedFieldCreator( GridPtr grid ) : PropertyFieldCreator( grid ){}


// Class Interface -----------------------------------------------------------------------------------------------
void HorizontallyDividedFieldCreator::appendFieldSector( double lowerYValue, double upperYValue, double propertyValue )
{
	this->lowerYValues.push_back( lowerYValue );
	this->upperYValues.push_back( upperYValue );
	this->propertyValues.push_back( propertyValue );
}


VectorPtr HorizontallyDividedFieldCreator::createPropertyFieldOnCells()
{
	VectorPtr field( new Vector );

	foreach( CellPtr cell, this->grid->getCells() )
	{
		double yPosition = cell->getCentroid()->y();
		double yPositionNorthFace = cell->getNorthFaceID()->getCentroid()->y();
		double yPositionSouthFace = cell->getSouthFaceID()->getCentroid()->y();

		bool positionFound = false;

		// Check if the cell is near any of the divisions between sectors
		this->proximityWithTheDivisionsBetweenSectorsChecker( positionFound, yPosition, yPositionNorthFace, yPositionSouthFace, field );

		// Check in which sector is located the cell
		this->simpleSectorLocationChecker( positionFound, yPosition, field );
	}

	return field;
}


VectorPtr HorizontallyDividedFieldCreator::createPropertyFieldOnFaces()
{
	VectorPtr field( new Vector );

	foreach( FacePtr face, this->grid->getInternalFaces() )
	{
		double yPosition = face->getCentroid()->y();

		if ( abs( face->getNormal()->y() ) == 1.0 ) // Horizontal faces
		{
			bool positionFound = false;

			// Check if the face is in the same position of any of the sector divisions
			this->coincidingPositionWithTheDivisionsBetweenSectorsChecker( positionFound, yPosition, field );

			// Check in which sector is located the face
			this->simpleSectorLocationChecker( positionFound, yPosition, field );
		}

		else // Vertical faces
		{
			double yPositionSouthVertex = ( ( face->getVertices() )[ 0 ] )->y();
			double yPositionNorthVertex = ( ( face->getVertices() )[ 1 ] )->y();

			bool positionFound = false;

			// Check if the face is near any of the divisions between sectors
			this->proximityWithTheDivisionsBetweenSectorsChecker( positionFound, yPosition, yPositionNorthVertex, yPositionSouthVertex, field );

			// Check in which sector is located the face
			this->simpleSectorLocationChecker( positionFound, yPosition, field );
		}
	}

	foreach( FacePtr face, this->grid->getExternalFaces() )
	{
		double yPosition = face->getCentroid()->y();

		if ( abs( face->getNormal()->y() ) == 1.0 ) // Horizontal faces
		{
			bool positionFound = false;

			// Check if the face is in the same position of any of the sector divisions
			this->coincidingPositionWithTheDivisionsBetweenSectorsChecker( positionFound, yPosition, field );

			// Check in which sector is located the face
			this->simpleSectorLocationChecker( positionFound, yPosition, field );
		}

		else // Vertical faces
		{
			double yPositionSouthVertex = ( ( face->getVertices() )[ 0 ] )->y();
			double yPositionNorthVertex = ( ( face->getVertices() )[ 1 ] )->y();

			bool positionFound = false;

			// Check if the face is near any of the divisions between sectors
			this->proximityWithTheDivisionsBetweenSectorsChecker( positionFound, yPosition, yPositionNorthVertex, yPositionSouthVertex, field );

			// Check in which sector is located the face
			this->simpleSectorLocationChecker( positionFound, yPosition, field );
		}
	}

	return field;
}


VectorPtr HorizontallyDividedFieldCreator::createPropertyFieldOnVertices()
{
	VectorPtr field( new Vector );

	foreach( VertexPtr vertex, this->grid->getVertices() )
	{
		double yPosition = vertex->y();

		bool positionFound = false;

		// Check if the vertex is in the same position of any of the sector divisions
		this->coincidingPositionWithTheDivisionsBetweenSectorsChecker( positionFound, yPosition, field );

		// Check in which sector is located the face
		this->simpleSectorLocationChecker( positionFound, yPosition, field );
	}

	return field;
}


// Private Member Functions --------------------------------------------------------------------------------------
void HorizontallyDividedFieldCreator::simpleSectorLocationChecker( bool& positionFound, double& yPosition, VectorPtr field )
{
	int i = 0;

	while ( positionFound == false && i < ( this->lowerYValues ).size() )
	{
		if ( yPosition >= this->lowerYValues[ i ] && yPosition <= this->upperYValues[ i ] )
		{
			( *field ).push_back( this->propertyValues[ i ] );

			positionFound = true;
		}

		i += 1;
	}
}


void HorizontallyDividedFieldCreator::proximityWithTheDivisionsBetweenSectorsChecker( bool& positionFound, double& yPosition, double& yPositionNorth, double& yPositionSouth, VectorPtr field )
{
	double dy = yPositionNorth - yPositionSouth;
	int i = 1;

	while ( positionFound == false && i < ( this->lowerYValues ).size() )
	{
		if ( abs( this->lowerYValues[ i ] - yPosition ) < ( dy / 2 ) )
		{
			double propertyInterpolatedValue = abs( ( yPositionNorth - this->lowerYValues[ i ] ) / dy ) * this->propertyValues[ i ] +
				abs( ( yPositionSouth - this->lowerYValues[ i ] ) / dy ) * this->propertyValues[ i - 1 ];
			( *field ).push_back( propertyInterpolatedValue );

			positionFound = true;
		}

		i += 1;
	}
}


void HorizontallyDividedFieldCreator::coincidingPositionWithTheDivisionsBetweenSectorsChecker( bool& positionFound, double& yPosition, VectorPtr field )
{
	int i = 1;

	while ( positionFound == false && i < ( this->lowerYValues ).size() )
	{
		if ( yPosition == ( this->lowerYValues )[ i ] )
		{
			double propertyInterpolatedValue = ( this->propertyValues[ i - 1 ] + this->propertyValues[ i ] ) / 2;
			( *field ).push_back( propertyInterpolatedValue );

			positionFound = true;
		}

		i += 1;
	}
}