#ifndef VERTICALLY_DIVIDED_FIELD_CREATOR_H
#define VERTICALLY_DIVIDED_FIELD_CREATOR_H

#include "../PropertyFieldCreator.h"

// CLASS DEFINITION ==============================================================================================
class VerticallyDividedFieldCreator : public PropertyFieldCreator
{
protected:

	// Data Members ----------------------------------------------------------------------------------------------
	Vector leftXValues;
	Vector rightXValues;
	Vector propertyValues;
	
public:

	// Constructor and Destructor --------------------------------------------------------------------------------
	VerticallyDividedFieldCreator( GridPtr grid );
	virtual ~VerticallyDividedFieldCreator(){}

	// Class Interface -------------------------------------------------------------------------------------------
	void appendFieldSector( double leftXValue, double rightXValue, double propertyValue );
	VectorPtr createPropertyFieldOnCells();
	VectorPtr createPropertyFieldOnFaces();
	VectorPtr createPropertyFieldOnVertices();

private:

	// Private Member Functions ----------------------------------------------------------------------------------
	void simpleSectorLocationChecker( bool& positionFound, double& xPosition, VectorPtr field );
	void proximityWithTheDivisionsBetweenSectorsChecker( bool& positionFound, double& xPosition, double& xPositionEast, double& xPositionWest, VectorPtr field );
	void coincidingPositionWithTheDivisionsBetweenSectorsChecker( bool& positionFound, double& xPosition, VectorPtr field );

}; // class VerticallyDividedFieldCreator

typedef SharedPointer< VerticallyDividedFieldCreator > VerticallyDividedFieldCreatorPtr;

#endif