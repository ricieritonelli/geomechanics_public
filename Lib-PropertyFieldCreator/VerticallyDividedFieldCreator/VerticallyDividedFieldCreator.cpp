#include "VerticallyDividedFieldCreator.h"

// Constructor ---------------------------------------------------------------------------------------------------
VerticallyDividedFieldCreator::VerticallyDividedFieldCreator( GridPtr grid ) : PropertyFieldCreator( grid ){}


// Class Interface -----------------------------------------------------------------------------------------------
void VerticallyDividedFieldCreator::appendFieldSector( double leftXValue, double rightXValue, double propertyValue )
{
	this->leftXValues.push_back( leftXValue );
	this->rightXValues.push_back( rightXValue );
	this->propertyValues.push_back( propertyValue );
}


VectorPtr VerticallyDividedFieldCreator::createPropertyFieldOnCells()
{
	VectorPtr field( new Vector );

	foreach( CellPtr cell, this->grid->getCells() )
	{
		double xPosition = cell->getCentroid()->x();
		double xPositionEastFace = cell->getEastFaceID()->getCentroid()->x();
		double xPositionWestFace = cell->getWestFaceID()->getCentroid()->x();

		bool positionFound = false;

		// Check if the cell is near any of the divisions between sectors
		this->proximityWithTheDivisionsBetweenSectorsChecker( positionFound, xPosition, xPositionEastFace, xPositionWestFace, field );

		// Check in which sector is located the cell
		this->simpleSectorLocationChecker( positionFound, xPosition, field );
	}

	return field;
}


VectorPtr VerticallyDividedFieldCreator::createPropertyFieldOnFaces()
{
	VectorPtr field( new Vector );

	foreach( FacePtr face, this->grid->getInternalFaces() )
	{
		double xPosition = face->getCentroid()->x();

		if ( abs( face->getNormal()->x() ) == 1.0 ) // Vertical faces
		{
			bool positionFound = false;

			// Check if the face is in the same position of any of the sector divisions
			this->coincidingPositionWithTheDivisionsBetweenSectorsChecker( positionFound, xPosition, field );

			// Check in which sector is located the face
			this->simpleSectorLocationChecker( positionFound, xPosition, field );
		}

		else // Horizontal faces
		{
			double xPositionWestVertex = ( ( face->getVertices() )[ 0 ] )->x();
			double xPositionEastVertex = ( ( face->getVertices() )[ 1 ] )->x();

			bool positionFound = false;

			// Check if the face is near any of the divisions between sectors
			this->proximityWithTheDivisionsBetweenSectorsChecker( positionFound, xPosition, xPositionEastVertex, xPositionWestVertex, field );

			// Check in which sector is located the face
			this->simpleSectorLocationChecker( positionFound, xPosition, field );
		}
	}

	foreach( FacePtr face, this->grid->getExternalFaces() )
	{
		double xPosition = face->getCentroid()->x();

		if ( abs( face->getNormal()->x() ) == 1.0 ) // Vertical faces
		{
			bool positionFound = false;

			// Check if the face is in the same position of any of the sector divisions
			this->coincidingPositionWithTheDivisionsBetweenSectorsChecker( positionFound, xPosition, field );

			// Check in which sector is located the face
			this->simpleSectorLocationChecker( positionFound, xPosition, field );
		}

		else // Horizontal faces
		{
			double xPositionWestVertex = ( ( face->getVertices() )[ 0 ] )->x();
			double xPositionEastVertex = ( ( face->getVertices() )[ 1 ] )->x();

			bool positionFound = false;

			// Check if the face is near any of the divisions between sectors
			this->proximityWithTheDivisionsBetweenSectorsChecker( positionFound, xPosition, xPositionEastVertex, xPositionWestVertex, field );

			// Check in which sector is located the face
			this->simpleSectorLocationChecker( positionFound, xPosition, field );
		}
	}

	return field;
}


VectorPtr VerticallyDividedFieldCreator::createPropertyFieldOnVertices()
{
	VectorPtr field( new Vector );

	foreach( VertexPtr vertex, this->grid->getVertices() )
	{
		double xPosition = vertex->x();

		bool positionFound = false;

		// Check if the vertex is in the same position of any of the sector divisions
		this->coincidingPositionWithTheDivisionsBetweenSectorsChecker( positionFound, xPosition, field );

		// Check in which sector is located the face
		this->simpleSectorLocationChecker( positionFound, xPosition, field );
	}

	return field;
}


// Private Member Functions --------------------------------------------------------------------------------------
void VerticallyDividedFieldCreator::simpleSectorLocationChecker( bool& positionFound, double& xPosition, VectorPtr field )
{
	int i = 0;

	while ( positionFound == false && i < ( this->leftXValues ).size() )
	{
		if ( xPosition >= this->leftXValues[ i ] && xPosition <= this->rightXValues[ i ] )
		{
			( *field ).push_back( this->propertyValues[ i ] );

			positionFound = true;
		}

		i += 1;
	}
}


void VerticallyDividedFieldCreator::proximityWithTheDivisionsBetweenSectorsChecker( bool& positionFound, double& xPosition, double& xPositionEast, double& xPositionWest, VectorPtr field )
{
	double dx = xPositionEast - xPositionWest;
	int i = 1;

	while ( positionFound == false && i < ( this->leftXValues ).size() )
	{
		if ( abs( this->leftXValues[ i ] - xPosition ) < ( dx / 2 ) )
		{
			double propertyInterpolatedValue = abs( ( xPositionEast - this->leftXValues[ i ] ) / dx ) * this->propertyValues[ i ] +
				abs( ( xPositionWest - this->leftXValues[ i ] ) / dx ) * this->propertyValues[ i - 1 ];
			( *field ).push_back( propertyInterpolatedValue );

			positionFound = true;
		}

		i += 1;
	}
}


void VerticallyDividedFieldCreator::coincidingPositionWithTheDivisionsBetweenSectorsChecker( bool& positionFound, double& xPosition, VectorPtr field )
{
	int i = 1;

	while ( positionFound == false && i < ( this->leftXValues ).size() )
	{
		if ( xPosition == ( this->leftXValues )[ i ] )
		{
			double propertyInterpolatedValue = ( this->propertyValues[ i - 1 ] + this->propertyValues[ i ] ) / 2;
			( *field ).push_back( propertyInterpolatedValue );

			positionFound = true;
		}

		i += 1;
	}
}