project "Lib-PropertyFieldCreator"

kind "StaticLib"
targetdir "../lib"

includedirs{
	"./"
}

links{
	"Lib-Utils",
	"Lib-CartesianGrid",
	"Lib-ScriptSystem"
}	

files {
	"**.cpp",
	"**.h"
}

