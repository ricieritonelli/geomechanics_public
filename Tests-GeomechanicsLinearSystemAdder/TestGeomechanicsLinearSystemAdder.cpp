#define BOOST_TEST_MODULE TestModule

//===================================================================

// GeoSegregated Adders TESTS
// GRID 2 x 2

//===================================================================

#include "Lib-VariableComputer/Lib-VariableComputer.h"

struct GeoLinSisAdderFixture
{
	GridPtr grid;
	SparseMatrixPtr pressureMatrix;
	VectorPtr pressureIndependent;
	SparseMatrixPtr displacementMatrix;
	VectorPtr displacementIndependent;
	
	GeoLinSisAdderFixture()
	{
		CartesianGridBuilderPtr builder( new CartesianGridBuilder() );
		int nx = 2;
		int ny = 2;
		double dx = 1;
		double dy = 1;
		this->grid = builder->build( nx, dx, ny, dy );
		this->pressureMatrix = SparseMatrixPtr( new SparseMatrix( ( this->grid->getCells() ).size() ) );
		this->pressureIndependent = VectorPtr( new Vector( ( this->grid->getCells() ).size() ) );
		this->displacementMatrix = SparseMatrixPtr( new SparseMatrix( this->grid->getNumberOfFaces() ) );
		this->displacementIndependent = VectorPtr( new Vector( this->grid->getNumberOfFaces() ) );
	}
};


FixtureTestSuite( GeoLinSisAdderTestSuite, GeoLinSisAdderFixture );

	// PRESSURE ===================================================================================================

	TestCase( GeoSegregatedSinglePhaseFluxAdderTest )
	{
		FieldInterpolatorPtr permeabilityInterpolator( new ConstantInterpolator( 4.0 ) );
		FieldInterpolatorPtr viscosityInterpolator( new ConstantInterpolator( 1.0 ) );

		GeoSegregatedSinglePhaseFluxAdder adder( pressureMatrix, permeabilityInterpolator, viscosityInterpolator, grid );
		adder.add();

		checkClose( ( *pressureMatrix )( 0, 0 ), 8.0, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 0, 1 ), -4.0, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 0, 2 ), -4.0, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 0, 3 ), 0.0, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 1, 0 ), -4.0, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 1, 1 ), 8.0, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 1, 2 ), 0.0, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 1, 3 ), -4.0, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 2, 0 ), -4.0, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 2, 1 ), 0.0, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 2, 2 ), 8.0, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 2, 3 ), -4.0, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 3, 0 ), 0.0, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 3, 1 ), -4.0, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 3, 2 ), -4.0, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 3, 3 ), 8.0, 1.0e-12 );
	}


	TestCase( GeoSegregatedSinglePhaseGravityFluxAdderTest )
	{
		FieldInterpolatorPtr permeabilityInterpolator( new ConstantInterpolator( 2.0 ) );
		FieldInterpolatorPtr viscosityInterpolator( new ConstantInterpolator( 1.0 ) );
		FieldInterpolatorPtr fluidDensityInterpolator( new ConstantInterpolator( 4.0 ) );
		double gravityAcceleration = -10.0;

		GeoSegregatedSinglePhaseGravityFluxAdder adder( pressureIndependent, permeabilityInterpolator, viscosityInterpolator, fluidDensityInterpolator, gravityAcceleration, grid );
		adder.add();

		checkClose( ( *pressureIndependent )[ 0 ], 80.0, 1.0e-12 );
		checkClose( ( *pressureIndependent )[ 1 ], 80.0, 1.0e-12 );
		checkClose( ( *pressureIndependent )[ 2 ], -80.0, 1.0e-12 );
		checkClose( ( *pressureIndependent )[ 3 ], -80.0, 1.0e-12 );
	}


	TestCase( GeoSegregatedSinglePhaseCompressibilityAdderTest )
	{
		VectorPtr pressureFieldOld( new Vector( 4, 10.0 ) );

		VectorPtr porosity( new Vector( 4, 0.0 ) );
		( *porosity )[ 0 ] = 0.1;
		( *porosity )[ 1 ] = 0.2;
		( *porosity )[ 2 ] = 0.3;
		( *porosity )[ 3 ] = 0.4;

		FieldInterpolatorPtr porosityInterpolator( new CellBasedInterpolator( porosity ) );
		FieldInterpolatorPtr solidPhaseCompressibilityInterpolator( new ConstantInterpolator( 0.5 ) );
		FieldInterpolatorPtr fluidPhaseCompressibilityInterpolator( new ConstantInterpolator( 0.1 ) );
		FieldInterpolatorPtr BiotCoefficientInterpolator( new ConstantInterpolator( 0.6 ) );
		double timeStep = 1.0;

		GeoSegregatedSinglePhaseCompressibilityAdder adder( pressureMatrix, pressureIndependent, pressureFieldOld, porosityInterpolator, solidPhaseCompressibilityInterpolator, fluidPhaseCompressibilityInterpolator, BiotCoefficientInterpolator, timeStep, grid );
		adder.add();

		checkClose( ( *pressureMatrix )( 0, 0 ), 0.26, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 1, 1 ), 0.22, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 2, 2 ), 0.18, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 3, 3 ), 0.14, 1.0e-12 );
		checkClose( ( *pressureIndependent )[ 0 ], 2.6, 1.0e-12 );
		checkClose( ( *pressureIndependent )[ 1 ], 2.2, 1.0e-12 );
		checkClose( ( *pressureIndependent )[ 2 ], 1.8, 1.0e-12 );
		checkClose( ( *pressureIndependent )[ 3 ], 1.4, 1.0e-12 );
	}


	TestCase( GeoSegregatedSinglePhaseVolumetricDeformationAdderTest )
	{
		/////////////////////////////////////////// Displacement
		//   --------7----------------7-------   // Field
		//   |               |               |   // Old
		//   |               |               |   //
		//   0               1               3   //
		//   |               |               |   //
		//   |               |               |   //
		//   --------3----------------3-------   //
		//   |               |               |   //
		//   |               |               |   //
		//   0               1               3   //
		//   |               |               |   //
		//   |               |               |   //
		//   --------0----------------0-------   //
		///////////////////////////////////////////

		/////////////////////////////////////////// Displacement
		//   -------3.5--------------3.5------   // Field
		//   |               |               |   //
		//   |               |               |   //
		//   0              0.5             1.5  //
		//   |               |               |   //
		//   |               |               |   //
		//   -------1.5--------------1.5------   //
		//   |               |               |   //
		//   |               |               |   //
		//   0              0.5             1.5  //
		//   |               |               |   //
		//   |               |               |   //
		//   --------0----------------0-------   //
		///////////////////////////////////////////

		FieldInterpolatorPtr BiotCoefficientInterpolator( new ConstantInterpolator( 0.5 ) );
		double timeStep = 1.0;

		VectorPtr displacementField( new Vector( 12, 0.0 ) );
		( *displacementField )[ 0 ] = 0.5;
		( *displacementField )[ 1 ] = 0.5;
		( *displacementField )[ 2 ] = 1.5;
		( *displacementField )[ 3 ] = 1.5;
		( *displacementField )[ 4 ] = 0.0;
		( *displacementField )[ 5 ] = 0.0;
		( *displacementField )[ 6 ] = 1.5;
		( *displacementField )[ 7 ] = 1.5;
		( *displacementField )[ 8 ] = 0.0;
		( *displacementField )[ 9 ] = 0.0;
		( *displacementField )[ 10 ] = 3.5;
		( *displacementField )[ 11 ] = 3.5;

		VectorPtr displacementFieldOld( new Vector( 12, 0.0 ) );
		( *displacementFieldOld )[ 0 ] = 1.0;
		( *displacementFieldOld )[ 1 ] = 1.0;
		( *displacementFieldOld )[ 2 ] = 3.0;
		( *displacementFieldOld )[ 3 ] = 3.0;
		( *displacementFieldOld )[ 4 ] = 0.0;
		( *displacementFieldOld )[ 5 ] = 0.0;
		( *displacementFieldOld )[ 6 ] = 3.0;
		( *displacementFieldOld )[ 7 ] = 3.0;
		( *displacementFieldOld )[ 8 ] = 0.0;
		( *displacementFieldOld )[ 9 ] = 0.0;
		( *displacementFieldOld )[ 10 ] = 7.0;
		( *displacementFieldOld )[ 11 ] = 7.0;

		GeoSegregatedSinglePhaseVolumetricDeformationAdder adder( pressureIndependent, displacementField, displacementFieldOld, BiotCoefficientInterpolator, timeStep, grid );
		adder.add();

		checkClose( ( *pressureIndependent )[ 0 ], 1.0, 1.0e-12 );
		checkClose( ( *pressureIndependent )[ 1 ], 1.25, 1.0e-12 );
		checkClose( ( *pressureIndependent )[ 2 ], 1.25, 1.0e-12 );
		checkClose( ( *pressureIndependent )[ 3 ], 1.5, 1.0e-12 );
	}


	TestCase( GeoSegregatedSinglePhaseConstantPressureAdderWestTest )
	{
		FieldInterpolatorPtr permeabilityInterpolator( new ConstantInterpolator( 4.0 ) );
		FieldInterpolatorPtr viscosityInterpolator( new ConstantInterpolator( 2.0 ) );
		double pressureWest = 10.0;

		GeoSegregatedSinglePhaseConstantPressureAdder adder( pressureMatrix, pressureIndependent, permeabilityInterpolator, viscosityInterpolator, grid, ( grid->getBoundaries() )[ 0 ], pressureWest );
		adder.add();

		checkClose( ( *pressureMatrix )( 0, 0 ), 4.0, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 2, 2 ), 4.0, 1.0e-12 );
		checkClose( ( *pressureIndependent )[ 0 ], 40.0, 1.0e-12 );
		checkClose( ( *pressureIndependent )[ 2 ], 40.0, 1.0e-12 );
	}


	TestCase( GeoSegregatedSinglePhaseConstantPressureAdderSouthTest )
	{
		FieldInterpolatorPtr permeabilityInterpolator( new ConstantInterpolator( 6.0 ) );
		FieldInterpolatorPtr viscosityInterpolator( new ConstantInterpolator( 2.0 ) );
		double pressureSouth = 10.0;

		GeoSegregatedSinglePhaseConstantPressureAdder adder( pressureMatrix, pressureIndependent, permeabilityInterpolator, viscosityInterpolator, grid, ( grid->getBoundaries() )[ 2 ], pressureSouth );
		adder.add();

		checkClose( ( *pressureMatrix )( 0, 0 ), 6.0, 1.0e-12 );
		checkClose( ( *pressureMatrix )( 1, 1 ), 6.0, 1.0e-12 );
		checkClose( ( *pressureIndependent )[ 0 ], 60.0, 1.0e-12 );
		checkClose( ( *pressureIndependent )[ 1 ], 60.0, 1.0e-12 );
	}


	TestCase( GeoSegregatedSinglePhaseConstantFluxAdderWestTest )
	{
		double fluxWest = 8.0;

		GeoSegregatedSinglePhaseConstantFluxAdder adder( pressureIndependent, ( grid->getBoundaries() )[ 0 ], fluxWest );
		adder.add();

		checkClose( ( *pressureIndependent )[ 0 ], 8.0, 1.0e-12 );
		checkClose( ( *pressureIndependent )[ 2 ], 8.0, 1.0e-12 );
	}


	TestCase( GeoSegregatedSinglePhaseConstantFluxAdderSouthTest )
	{
		double fluxSouth = 6.0;

		GeoSegregatedSinglePhaseConstantFluxAdder adder( pressureIndependent, ( grid->getBoundaries() )[ 2 ], fluxSouth );
		adder.add();

		checkClose( ( *pressureIndependent )[ 0 ], 6.0, 1.0e-12 );
		checkClose( ( *pressureIndependent )[ 1 ], 6.0, 1.0e-12 );
	}

	// DISPLACEMENT ==========================================================================================

	TestCase( GeoSegregatedHorDisplacementCoeffAdderTest )
	{
		/////////////////////////////////////////// Shear
		//   3---------------3---------------3   // Modulus
		//   |               |               |   //
		//   |               |               |   // and
		//   |       20      |       20      |   //
		//   |               |               |   // Lame's
		//   |               |               |   // Parameter
		//   2---------------2---------------2   //
		//   |               |               |   //
		//   |               |               |   //
		//   |       10      |       10      |   //
		//   |               |               |   //
		//   |               |               |   //
		//   1---------------1---------------1   //
		///////////////////////////////////////////

		FieldInterpolatorPtr PoissonsRatioInterpolator( new ConstantInterpolator( 0.2 ) );

		VectorPtr shearModulus( new Vector( 9, 0.0 ) );
		( *shearModulus )[ 0 ] = 1.0;
		( *shearModulus )[ 1 ] = 1.0;
		( *shearModulus )[ 2 ] = 1.0;
		( *shearModulus )[ 3 ] = 2.0;
		( *shearModulus )[ 4 ] = 2.0;
		( *shearModulus )[ 5 ] = 2.0;
		( *shearModulus )[ 6 ] = 3.0;
		( *shearModulus )[ 7 ] = 3.0;
		( *shearModulus )[ 8 ] = 3.0;
		FieldInterpolatorPtr shearModulusInterpolator( new VertexBasedInterpolator( shearModulus ) );

		VectorPtr LamesParameter( new Vector( 4, 0.0 ) );
		( *LamesParameter )[ 0 ] = 10.0;
		( *LamesParameter )[ 1 ] = 10.0;
		( *LamesParameter )[ 2 ] = 20.0;
		( *LamesParameter )[ 3 ] = 20.0;
		FieldInterpolatorPtr LamesParameterInterpolator( new CellBasedInterpolator( LamesParameter ) );

		GeoSegregatedHorDisplacementCoeffAdder adder( displacementMatrix, PoissonsRatioInterpolator, shearModulusInterpolator, LamesParameterInterpolator, grid );
		adder.add();

		checkClose( ( *displacementMatrix )( 4, 0 ), -40.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 4, 5 ), -1.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 4, 4 ), 41.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 4, 2 ), -10.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 4, 8 ), 10.0, 1.0e-12 );

		checkClose( ( *displacementMatrix )( 0, 4 ), -40.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 0, 6 ), -40.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 0, 1 ), -2.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 0, 0 ), 82.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 0, 3 ), -12.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 0, 9 ), 10.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 0, 2 ), 12.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 0, 8 ), -10.0, 1.0e-12 );

		checkClose( ( *displacementMatrix )( 6, 0 ), -40.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 6, 7 ), -1.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 6, 6 ), 41.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 6, 3 ), 10.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 6, 9 ), -10.0, 1.0e-12 );

		checkClose( ( *displacementMatrix )( 5, 1 ), -80.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 5, 4 ), -1.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 5, 5 ), 81.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 5, 10 ), -20.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 5, 2 ), 20.0, 1.0e-12 );

		checkClose( ( *displacementMatrix )( 1, 7 ), -80.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 1, 5 ), -80.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 1, 0 ), -2.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 1, 1 ), 162.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 1, 10 ), 20.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 1, 2 ), -22.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 1, 11 ), -20.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 1, 3 ), 22.0, 1.0e-12 );

		checkClose( ( *displacementMatrix )( 7, 1 ), -80.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 7, 6 ), -1.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 7, 7 ), 81.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 7, 11 ), 20.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 7, 3 ), -20.0, 1.0e-12 );
	}


	TestCase( GeoSegregatedHorDisplacementPressureAdderTest )
	{
		FieldInterpolatorPtr BiotCoefficientInterpolator( new ConstantInterpolator( 0.6 ) );

		VectorPtr pressureField( new Vector( 4, 0.0 ) );
		( *pressureField )[ 0 ] = 1.0;
		( *pressureField )[ 1 ] = 3.0;
		( *pressureField )[ 2 ] = 6.0;
		( *pressureField )[ 3 ] = 10.0;

		GeoSegregatedHorDisplacementPressureAdder adder( displacementIndependent, pressureField, BiotCoefficientInterpolator, grid );
		adder.add();

		checkClose( ( *displacementIndependent )[ 4 ], -0.6, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 0 ], -1.2, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 6 ], 1.8, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 5 ], -3.6, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 1 ], -2.4, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 7 ], 6.0, 1.0e-12 );
	}


	TestCase( GeoSegregatedVertDisplacementCoeffAdderTest )
	{
		/////////////////////////////////////////// Shear
		//   3---------------3---------------3   // Modulus
		//   |               |               |   //
		//   |               |               |   // and
		//   |       20      |       20      |   //
		//   |               |               |   // Lame's
		//   |               |               |   // Parameter
		//   2---------------2---------------2   //
		//   |               |               |   //
		//   |               |               |   //
		//   |       10      |       10      |   //
		//   |               |               |   //
		//   |               |               |   //
		//   1---------------1---------------1   //
		///////////////////////////////////////////

		FieldInterpolatorPtr PoissonsRatioInterpolator( new ConstantInterpolator( 0.2 ) );

		VectorPtr shearModulus( new Vector( 9, 0.0 ) );
		( *shearModulus )[ 0 ] = 1.0;
		( *shearModulus )[ 1 ] = 1.0;
		( *shearModulus )[ 2 ] = 1.0;
		( *shearModulus )[ 3 ] = 2.0;
		( *shearModulus )[ 4 ] = 2.0;
		( *shearModulus )[ 5 ] = 2.0;
		( *shearModulus )[ 6 ] = 3.0;
		( *shearModulus )[ 7 ] = 3.0;
		( *shearModulus )[ 8 ] = 3.0;
		FieldInterpolatorPtr shearModulusInterpolator( new VertexBasedInterpolator( shearModulus ) );

		VectorPtr LamesParameter( new Vector( 4, 0.0 ) );
		( *LamesParameter )[ 0 ] = 10.0;
		( *LamesParameter )[ 1 ] = 10.0;
		( *LamesParameter )[ 2 ] = 20.0;
		( *LamesParameter )[ 3 ] = 20.0;
		FieldInterpolatorPtr LamesParameterInterpolator( new CellBasedInterpolator( LamesParameter ) );

		GeoSegregatedVertDisplacementCoeffAdder adder( displacementMatrix, PoissonsRatioInterpolator, shearModulusInterpolator, LamesParameterInterpolator, grid);
		adder.add();

		checkClose( ( *displacementMatrix )( 8, 2 ), -40.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 8, 9 ), -0.5, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 8, 8 ), 40.5, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 8, 4 ), 10.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 8, 0 ), -10.0, 1.0e-12 );

		checkClose( ( *displacementMatrix )( 9, 3 ), -40.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 9, 8 ), -0.5, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 9, 9 ), 40.5, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 9, 0 ), 10.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 9, 6 ), -10.0, 1.0e-12 );

		checkClose( ( *displacementMatrix )( 2, 8 ), -40.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 2, 10 ), -80.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 2, 3 ), -2.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 2, 2 ), 122.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 2, 4 ), -10.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 2, 5 ), 20.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 2, 0 ), 12.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 2, 1 ), -22.0, 1.0e-12 );

		checkClose( ( *displacementMatrix )( 3, 9 ), -40.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 3, 11 ), -80.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 3, 2 ), -2.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 3, 3 ), 122.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 3, 0 ), -12.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 3, 1 ), 22.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 3, 6 ), 10.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 3, 7 ), -20.0, 1.0e-12 );

		checkClose( ( *displacementMatrix )( 10, 2 ), -80.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 10, 11 ), -1.5, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 10, 10 ), 81.5, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 10, 5 ), -20.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 10, 1 ), 20.0, 1.0e-12 );

		checkClose( ( *displacementMatrix )( 11, 3 ), -80.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 11, 10 ), -1.5, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 11, 11 ), 81.5, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 11, 1 ), -20.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 11, 7 ), 20.0, 1.0e-12 );
	}


	TestCase( GeoSegregatedVertDisplacementPressureAdderTest )
	{
		FieldInterpolatorPtr BiotCoefficientInterpolator( new ConstantInterpolator( 0.6 ) );

		VectorPtr pressureField( new Vector( 4, 0.0 ) );
		( *pressureField )[ 0 ] = 1.0;
		( *pressureField )[ 1 ] = 3.0;
		( *pressureField )[ 2 ] = 6.0;
		( *pressureField )[ 3 ] = 10.0;

		GeoSegregatedVertDisplacementPressureAdder adder( displacementIndependent, pressureField, BiotCoefficientInterpolator, grid);
		adder.add();

		checkClose( ( *displacementIndependent )[ 8 ], -0.6, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 9 ], -1.8, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 2 ], -3.0, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 3 ], -4.2, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 10 ], 3.6, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 11 ], 6.0, 1.0e-12 );
	}


	TestCase( GeoSegregatedVertDisplacementGravityAdderTest )
	{
		double gravityAcceleration = -10.0;
		FieldInterpolatorPtr fluidDensityInterpolator( new ConstantInterpolator( 1000.0 ) );
		FieldInterpolatorPtr solidDensityInterpolator( new ConstantInterpolator( 2000.0 ) );

		VectorPtr porosity( new Vector( 4, 0.0 ) );
		( *porosity )[ 0 ] = 0.2;
		( *porosity )[ 1 ] = 0.4;
		( *porosity )[ 2 ] = 0.6;
		( *porosity )[ 3 ] = 0.8;
		FieldInterpolatorPtr porosityInterpolator( new CellBasedInterpolator( porosity ) );

		GeoSegregatedVertDisplacementGravityAdder adder( displacementIndependent, fluidDensityInterpolator, solidDensityInterpolator, porosityInterpolator, gravityAcceleration, grid );
		adder.add();

		checkClose( ( *displacementIndependent )[ 8 ], -9000.0, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 9 ], -8000.0, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 2 ], -16000.0, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 3 ], -14000.0, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 10 ], -7000.0, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 11 ], -6000.0, 1.0e-12 );
	}


	TestCase( GeoSegregatedConstHorDisplacementAdderWestTest )
	{
		// Filling the matrix with elements = 2.0
		for ( int i = 0; i < grid->getNumberOfFaces(); ++i )
		{
			for ( int j = 0; j < grid->getNumberOfFaces(); ++j )
			{
				( *displacementMatrix )( i, j ) = 2.0;
			}
		}

		// Filling the independent vector with elements = 2.0
		for ( int i = 0; i < ( *displacementIndependent ).size(); ++i )
		{
			( *displacementIndependent )[ i ] = 2.0;
		}

		FieldInterpolatorPtr shearModulusInterpolator( new ConstantInterpolator( 4.0 ) );
		double horDisplacementWest = 5.0;

		GeoSegregatedConstHorDisplacementAdder adder( displacementMatrix, displacementIndependent, shearModulusInterpolator, grid, ( grid->getBoundaries() )[ 0 ], horDisplacementWest );
		adder.add();

		checkClose( ( *displacementIndependent )[ 4 ], 5.0, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 5 ], 5.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 4, 4 ), 1.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 5, 5 ), 1.0, 1.0e-12 );

		checkClose( ( *displacementMatrix )( 4, 0 ), 0.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 4, 5 ), 0.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 4, 8 ), 0.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 4, 2 ), 0.0, 1.0e-12 );

		checkClose( ( *displacementMatrix )( 5, 4 ), 0.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 5, 1 ), 0.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 5, 2 ), 0.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 5, 10 ), 0.0, 1.0e-12 );
	}


	TestCase( GeoSegregatedConstHorDisplacementAdderSouthTest )
	{
		FieldInterpolatorPtr shearModulusInterpolator( new ConstantInterpolator( 4.0 ) );
		double horDisplacementSouth = 5.0;

		GeoSegregatedConstHorDisplacementAdder adder( displacementMatrix, displacementIndependent, shearModulusInterpolator, grid, ( grid->getBoundaries() )[ 2 ], horDisplacementSouth );
		adder.add();

		checkClose( ( *displacementMatrix )( 4, 4 ), 4.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 0, 0 ), 8.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 0, 8 ), -4.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 0, 9 ), 4.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 6, 6 ), 4.0, 1.0e-12 );

		checkClose( ( *displacementIndependent )[ 4 ], 20.0, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 0 ], 40.0, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 6 ], 20.0, 1.0e-12 );
	}


	TestCase( GeoSegregatedConstVertDisplacementAdderSouthTest )
	{
		// Filling the matrix with elements = 2.0
		for ( int i = 0; i < grid->getNumberOfFaces(); ++i )
		{
			for ( int j = 0; j < grid->getNumberOfFaces(); ++j )
			{
				( *displacementMatrix )( i, j ) = 2.0;
			}
		}

		// Filling the independent vector with elements = 2.0
		for ( int i = 0; i < ( *displacementIndependent ).size(); ++i )
		{
			( *displacementIndependent )[ i ] = 2.0;
		}

		FieldInterpolatorPtr shearModulusInterpolator( new ConstantInterpolator( 4.0 ) );
		double vertDisplacementSouth = 5.0;

		GeoSegregatedConstVertDisplacementAdder adder( displacementMatrix, displacementIndependent, shearModulusInterpolator, grid, ( grid->getBoundaries() )[ 2 ], vertDisplacementSouth );
		adder.add();

		checkClose( ( *displacementIndependent )[ 8 ], 5.0, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 9 ], 5.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 8, 8 ), 1.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 9, 9 ), 1.0, 1.0e-12 );

		checkClose( ( *displacementMatrix )( 8, 9 ), 0.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 8, 2 ), 0.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 8, 4 ), 0.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 8, 0 ), 0.0, 1.0e-12 );

		checkClose( ( *displacementMatrix )( 9, 8 ), 0.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 9, 3 ), 0.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 9, 0 ), 0.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 9, 6 ), 0.0, 1.0e-12 );
	}


	TestCase( GeoSegregatedConstVertDisplacementAdderWestTest )
	{
		FieldInterpolatorPtr shearModulusInterpolator( new ConstantInterpolator( 4.0 ) );
		double vertDisplacementWest = 5.0;

		GeoSegregatedConstVertDisplacementAdder adder( displacementMatrix, displacementIndependent, shearModulusInterpolator, grid, ( grid->getBoundaries() )[ 0 ], vertDisplacementWest );
		adder.add();

		checkClose( ( *displacementMatrix )( 8, 8 ), 4.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 2, 2 ), 8.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 2, 4 ), -4.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 2, 5 ), 4.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 10, 10 ), 4.0, 1.0e-12 );

		checkClose( ( *displacementIndependent )[ 8 ], 20.0, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 2 ], 40.0, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 10 ], 20.0, 1.0e-12 );
	}


	TestCase( GeoSegregatedConstNormalStressAdderWestTest )
	{
		double normalStressWest = 3.0;

		GeoSegregatedConstNormalStressAdder adder( displacementIndependent, ( grid->getBoundaries() )[ 0 ], normalStressWest );
		adder.add();

		checkClose( ( *displacementIndependent )[ 4 ], 3.0, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 5 ], 3.0, 1.0e-12 );
	}


	TestCase( GeoSegregatedConstNormalStressAdderSouthTest )
	{
		double normalStressSouth = 6.0;

		GeoSegregatedConstNormalStressAdder adder( displacementIndependent, ( grid->getBoundaries() )[ 2 ], normalStressSouth );
		adder.add();

		checkClose( ( *displacementIndependent )[ 8 ], 6.0, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 9 ], 6.0, 1.0e-12 );
	}


	TestCase( GeoSegregatedConstShearStressAdderWestTest )
	{
		double shearStressWest = 7.0;

		GeoSegregatedConstShearStressAdder adder( displacementIndependent, grid, ( grid->getBoundaries() )[ 0 ], shearStressWest );
		adder.add();

		checkClose( ( *displacementIndependent )[ 8 ], 3.5, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 2 ], 7.0, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 10 ], 3.5, 1.0e-12 );
	}


	TestCase( GeoSegregatedConstShearStressAdderSouthTest )
	{
		double shearStressSouth = 9.0;

		GeoSegregatedConstShearStressAdder adder( displacementIndependent, grid, ( grid->getBoundaries() )[ 2 ], shearStressSouth );
		adder.add();

		checkClose( ( *displacementIndependent )[ 4 ], 4.5, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 0 ], 9.0, 1.0e-12 );
		checkClose( ( *displacementIndependent )[ 6 ], 4.5, 1.0e-12 );
	}
	

	TestCase( GeoSegregatedConstForceMandelsProblemAdderWestTest )
	{
		double normalStressWest = 5.0;

		GeoSegregatedConstForceMandelsProblemAdder adder( this->displacementMatrix, this->displacementIndependent, ( this->grid->getBoundaries() )[ 0 ], this->grid, normalStressWest );
		adder.add();

		checkClose( ( *displacementIndependent )[ 5 ], 5.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 4, 4 ), 1.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 4, 5 ), -1.0, 1.0e-12 );
	}


	TestCase( GeoSegregatedConstForceMandelsProblemAdderNorthTest )
	{
		double normalStressNorth = 6.0;

		GeoSegregatedConstForceMandelsProblemAdder adder( this->displacementMatrix, this->displacementIndependent, ( this->grid->getBoundaries() )[ 3 ], this->grid, normalStressNorth );
		adder.add();

		checkClose( ( *displacementIndependent )[ 11 ], 6.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 10, 10 ), 1.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 10, 11 ), -1.0, 1.0e-12 );

		normalStressNorth = 15.0;
		( *displacementIndependent )[ 11 ] = 0;
		adder.add();

		checkClose( ( *displacementIndependent )[ 11 ], 15.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 10, 10 ), 1.0, 1.0e-12 );
		checkClose( ( *displacementMatrix )( 10, 11 ), -1.0, 1.0e-12 );
	}


TestSuiteEnd();