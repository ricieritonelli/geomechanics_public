project "Tests-PropertyFieldCreator"

kind "ConsoleApp"
targetdir "../tests"

includedirs{
	"./"
}

links {
	"Lib-PropertyFieldCreator",
	"Lib-CartesianGrid",
	"Lib-Utils",
}

files {
	"**.cpp",
	"**.h"
}

configuration "Release"
	flags{ "Unicode" }
configuration "Debug"
	flags{ "Unicode" }
