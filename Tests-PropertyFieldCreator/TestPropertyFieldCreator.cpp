#define BOOST_TEST_MODULE TestModule

#include "Lib-PropertyFieldCreator/Lib-PropertyFieldCreator.h"

TestCase( ConstantFieldCreatorTest )
{
	CartesianGridBuilder builder;
	int nx = 3;
	int ny = 6;
	double dx = 1.0;
	double dy = 1.0;

	GridPtr grid = builder.build( nx, dx, ny, dy );
	double propertyValue = 5.0;
	ConstantFieldCreatorPtr fieldCreator( new ConstantFieldCreator( grid, propertyValue ) );

	VectorPtr propertyFieldOnCells = fieldCreator->createPropertyFieldOnCells();
	VectorPtr propertyFieldOnFaces = fieldCreator->createPropertyFieldOnFaces();
	VectorPtr propertyFieldOnVertices = fieldCreator->createPropertyFieldOnVertices();

	checkEqual( propertyFieldOnCells->size(), 18 );
	checkClose( ( *propertyFieldOnCells )[ 0 ], 5.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnCells )[ 17 ], 5.0, 1.0e-12 );

	checkEqual( propertyFieldOnFaces->size(), 45 );
	checkClose( ( *propertyFieldOnFaces )[ 0 ], 5.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 44 ], 5.0, 1.0e-12 );

	checkEqual( propertyFieldOnVertices->size(), 28 );
	checkClose( ( *propertyFieldOnVertices )[ 0 ], 5.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 27 ], 5.0, 1.0e-12 );
}


TestCase( HorizontallyDividedFieldCreatorTest_1 )
{
//		----------------- Property = 2.0
//		|       |       |
//		|       |       |
//		-----------------
//		|       |       |
//		|       |       | ------ 2.25
//		-----------------
//		|       |       |
//		|       |       |
//		-----------------
//		|       |       |
//		|       |       |
//		----------------- Property = 1.0

	CartesianGridBuilder builder;
	int nx = 2;
	int ny = 4;
	double dx = 1.0;
	double dy = 1.0;

	GridPtr grid = builder.build( nx, dx, ny, dy );
	double lowerYValue_1 = 0.0;
	double upperYValues_1 = 2.25;
	double propertyValue_1 = 1.0;
	double lowerYValue_2 = 2.25;
	double upperYValues_2 = 4.0;
	double propertyValue_2 = 2.0;

	HorizontallyDividedFieldCreatorPtr fieldCreator( new HorizontallyDividedFieldCreator( grid ) );
	fieldCreator->appendFieldSector( lowerYValue_1, upperYValues_1, propertyValue_1 );
	fieldCreator->appendFieldSector( lowerYValue_2, upperYValues_2, propertyValue_2 );

	VectorPtr propertyFieldOnCells = fieldCreator->createPropertyFieldOnCells();

	checkEqual( propertyFieldOnCells->size(), 8 );
	checkClose( ( *propertyFieldOnCells )[ 0 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnCells )[ 1 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnCells )[ 2 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnCells )[ 3 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnCells )[ 4 ], 1.75, 1.0e-12 );
	checkClose( ( *propertyFieldOnCells )[ 5 ], 1.75, 1.0e-12 );
	checkClose( ( *propertyFieldOnCells )[ 6 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnCells )[ 7 ], 2.0, 1.0e-12 );
}


TestCase( HorizontallyDividedFieldCreatorTest_2 )
{
	//		----------------- Property = 2.0
	//		|       |       |
	//		|       |       |
	//		-----------------
	//		|       |       |
	//		|       |       | ------ 1.25
	//		-----------------
	//		|       |       |
	//		|       |       |
	//		----------------- Property = 1.0

	CartesianGridBuilder builder;
	int nx = 2;
	int ny = 3;
	double dx = 1.0;
	double dy = 1.0;

	GridPtr grid = builder.build( nx, dx, ny, dy );
	double lowerYValue_1 = 0.0;
	double upperYValues_1 = 1.25;
	double propertyValue_1 = 1.0;
	double lowerYValue_2 = 1.25;
	double upperYValues_2 = 3.0;
	double propertyValue_2 = 2.0;

	HorizontallyDividedFieldCreatorPtr fieldCreator( new HorizontallyDividedFieldCreator( grid ) );
	fieldCreator->appendFieldSector( lowerYValue_1, upperYValues_1, propertyValue_1 );
	fieldCreator->appendFieldSector( lowerYValue_2, upperYValues_2, propertyValue_2 );

	VectorPtr propertyFieldOnFaces = fieldCreator->createPropertyFieldOnFaces();

	checkEqual( propertyFieldOnFaces->size(), 17 );
	checkClose( ( *propertyFieldOnFaces )[ 0 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 1 ], 1.75, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 2 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 3 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 4 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 5 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 6 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 7 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 8 ], 1.75, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 9 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 10 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 11 ], 1.75, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 12 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 13 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 14 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 15 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 16 ], 2.0, 1.0e-12 );

	VectorPtr propertyFieldOnVertices = fieldCreator->createPropertyFieldOnVertices();

	checkEqual( propertyFieldOnVertices->size(), 12 );
	checkClose( ( *propertyFieldOnVertices )[ 0 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 1 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 2 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 3 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 4 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 5 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 6 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 7 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 8 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 9 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 10 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 11 ], 2.0, 1.0e-12 );
}


TestCase( HorizontallyDividedFieldCreatorTest_3 )
{
	//		----------------- Property = 2.0
	//		|       |       |
	//		|       |       |
	//		----------------- ------ 1.0
	//		|       |       |
	//		|       |       |
	//		----------------- Property = 1.0


	CartesianGridBuilder builder;
	int nx = 2;
	int ny = 2;
	double dx = 1.0;
	double dy = 1.0;

	GridPtr grid = builder.build( nx, dx, ny, dy );
	double lowerYValue_1 = 0.0;
	double upperYValues_1 = 1.0;
	double propertyValue_1 = 1.0;
	double lowerYValue_2 = 1.0;
	double upperYValues_2 = 2.0;
	double propertyValue_2 = 2.0;

	HorizontallyDividedFieldCreatorPtr fieldCreator( new HorizontallyDividedFieldCreator( grid ) );
	fieldCreator->appendFieldSector( lowerYValue_1, upperYValues_1, propertyValue_1 );
	fieldCreator->appendFieldSector( lowerYValue_2, upperYValues_2, propertyValue_2 );

	VectorPtr propertyFieldOnFaces = fieldCreator->createPropertyFieldOnFaces();

	checkEqual( propertyFieldOnFaces->size(), 12 );
	checkClose( ( *propertyFieldOnFaces )[ 0 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 1 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 2 ], 1.5, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 3 ], 1.5, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 4 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 5 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 6 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 7 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 8 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 9 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 10 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 11 ], 2.0, 1.0e-12 );

	VectorPtr propertyFieldOnVertices = fieldCreator->createPropertyFieldOnVertices();

	checkEqual( propertyFieldOnVertices->size(), 9 );
	checkClose( ( *propertyFieldOnVertices )[ 0 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 1 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 2 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 3 ], 1.5, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 4 ], 1.5, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 5 ], 1.5, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 6 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 7 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 8 ], 2.0, 1.0e-12 );
}


TestCase( VerticallyDividedFieldCreatorTest_1 )
{
	//		---------------------------------
	//		|       |       |       |       |
	//		|       |       |       |       |
	//		---------------------------------
	//		|       |       |       |       |
	//		|       |       |       |       |
	//		---------------------------------
	//		Prop = 1.0         |         Prop = 2.0
	//                         2.25


	CartesianGridBuilder builder;
	int nx = 4;
	int ny = 2;
	double dx = 1.0;
	double dy = 1.0;

	GridPtr grid = builder.build( nx, dx, ny, dy );
	double leftXValue_1 = 0.0;
	double rightXValues_1 = 2.25;
	double propertyValue_1 = 1.0;
	double leftXValue_2 = 2.25;
	double rightXValues_2 = 4.0;
	double propertyValue_2 = 2.0;

	VerticallyDividedFieldCreatorPtr fieldCreator( new VerticallyDividedFieldCreator( grid ) );
	fieldCreator->appendFieldSector( leftXValue_1, rightXValues_1, propertyValue_1 );
	fieldCreator->appendFieldSector( leftXValue_2, rightXValues_2, propertyValue_2 );

	VectorPtr propertyFieldOnCells = fieldCreator->createPropertyFieldOnCells();

	checkEqual( propertyFieldOnCells->size(), 8 );
	checkClose( ( *propertyFieldOnCells )[ 0 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnCells )[ 1 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnCells )[ 2 ], 1.75, 1.0e-12 );
	checkClose( ( *propertyFieldOnCells )[ 3 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnCells )[ 4 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnCells )[ 5 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnCells )[ 6 ], 1.75, 1.0e-12 );
	checkClose( ( *propertyFieldOnCells )[ 7 ], 2.0, 1.0e-12 );
}


TestCase( VerticallyDividedFieldCreatorTest_2 )
{
	//		-------------------------
	//		|       |       |       |
	//		|       |       |       |
	//		-------------------------
	//		|       |       |       |
	//		|       |       |       |
	//		-------------------------
	//		Prop = 1.0  |        Prop = 2.0
	//                  1.25

	CartesianGridBuilder builder;
	int nx = 3;
	int ny = 2;
	double dx = 1.0;
	double dy = 1.0;

	GridPtr grid = builder.build( nx, dx, ny, dy );
	double leftXValue_1 = 0.0;
	double rightXValues_1 = 1.25;
	double propertyValue_1 = 1.0;
	double leftXValue_2 = 1.25;
	double rightXValues_2 = 3.0;
	double propertyValue_2 = 2.0;

	VerticallyDividedFieldCreatorPtr fieldCreator( new VerticallyDividedFieldCreator( grid ) );
	fieldCreator->appendFieldSector( leftXValue_1, rightXValues_1, propertyValue_1 );
	fieldCreator->appendFieldSector( leftXValue_2, rightXValues_2, propertyValue_2 );

	VectorPtr propertyFieldOnFaces = fieldCreator->createPropertyFieldOnFaces();

	checkEqual( propertyFieldOnFaces->size(), 17 );
	checkClose( ( *propertyFieldOnFaces )[ 0 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 1 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 2 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 3 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 4 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 5 ], 1.75, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 6 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 7 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 8 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 9 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 10 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 11 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 12 ], 1.75, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 13 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 14 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 15 ], 1.75, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 16 ], 2.0, 1.0e-12 );

	VectorPtr propertyFieldOnVertices = fieldCreator->createPropertyFieldOnVertices();

	checkEqual( propertyFieldOnVertices->size(), 12 );
	checkClose( ( *propertyFieldOnVertices )[ 0 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 1 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 2 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 3 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 4 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 5 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 6 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 7 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 8 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 9 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 10 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 11 ], 2.0, 1.0e-12 );
}


TestCase( VerticallyDividedFieldCreatorTest_3 )
{
	//		-----------------
	//		|       |       |
	//		|       |       |
	//		-----------------
	//		|       |       |
	//		|       |       |
	//		-----------------
	//	Prop = 1.0  |    Prop = 2.0
	//              1.0

	CartesianGridBuilder builder;
	int nx = 2;
	int ny = 2;
	double dx = 1.0;
	double dy = 1.0;

	GridPtr grid = builder.build( nx, dx, ny, dy );
	double leftXValue_1 = 0.0;
	double rightXValues_1 = 1.0;
	double propertyValue_1 = 1.0;
	double leftXValue_2 = 1.0;
	double rightXValues_2 = 2.0;
	double propertyValue_2 = 2.0;

	VerticallyDividedFieldCreatorPtr fieldCreator( new VerticallyDividedFieldCreator( grid ) );
	fieldCreator->appendFieldSector( leftXValue_1, rightXValues_1, propertyValue_1 );
	fieldCreator->appendFieldSector( leftXValue_2, rightXValues_2, propertyValue_2 );

	VectorPtr propertyFieldOnFaces = fieldCreator->createPropertyFieldOnFaces();

	checkEqual( propertyFieldOnFaces->size(), 12 );
	checkClose( ( *propertyFieldOnFaces )[ 0 ], 1.5, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 1 ], 1.5, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 2 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 3 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 4 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 5 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 6 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 7 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 8 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 9 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 10 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnFaces )[ 11 ], 2.0, 1.0e-12 );

	VectorPtr propertyFieldOnVertices = fieldCreator->createPropertyFieldOnVertices();

	checkEqual( propertyFieldOnVertices->size(), 9 );
	checkClose( ( *propertyFieldOnVertices )[ 0 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 1 ], 1.5, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 2 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 3 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 4 ], 1.5, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 5 ], 2.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 6 ], 1.0, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 7 ], 1.5, 1.0e-12 );
	checkClose( ( *propertyFieldOnVertices )[ 8 ], 2.0, 1.0e-12 );
}