#define BOOST_TEST_MODULE TestModule

#include "Lib-VariableComputer/Lib-VariableComputer.h"

struct GeoVariableUpdaterFixture
{
	GridPtr grid;
	VectorPtr displacementField;
	VectorPtr displacementFieldOld;
	VectorPtr pressureField;
	VectorPtr pressureFieldOld;

	GeoVariableUpdaterFixture()
	{
		CartesianGridBuilderPtr builder( new CartesianGridBuilder() );
		int nx = 2;
		int ny = 2;
		double dx = 1;
		double dy = 1;
		this->grid = builder->build( nx, dx, ny, dy );

		/////////////////////////////////////////// Displacement
		//   -------0.4--------------0.4------   // Field
		//   |               |               |   // Old
		//   |               |               |   //
		//   0       0      0.3       0     0.6  // and
		//   |               |               |   //
		//   |               |               |   // Pressure
		//   -------0.2--------------0.2------   // Field
		//   |               |               |   // Old
		//   |               |               |   //
		//   0       0      0.3       0     0.6  //
		//   |               |               |   //
		//   |               |               |   //
		//   --------0----------------0-------   //
		///////////////////////////////////////////

		/////////////////////////////////////////// Displacement
		//   -------0.2--------------0.2------   // Field
		//   |               |               |   //
		//   |               |               |   // and
		//   0      20      0.2      20     0.4  //
		//   |               |               |   // Pressure
		//   |               |               |   // Field
		//   -------0.1--------------0.1------   //
		//   |               |               |   //
		//   |               |               |   //
		//   0      10      0.2      10     0.4  //
		//   |               |               |   //
		//   |               |               |   //
		//   --------0----------------0-------   //
		///////////////////////////////////////////

		this->displacementFieldOld = VectorPtr( new Vector( 12, 0.0 ) );
		( *displacementFieldOld )[ 0 ] = 0.3;
		( *displacementFieldOld )[ 1 ] = 0.3;
		( *displacementFieldOld )[ 2 ] = 0.2;
		( *displacementFieldOld )[ 3 ] = 0.2;
		( *displacementFieldOld )[ 4 ] = 0.0;
		( *displacementFieldOld )[ 5 ] = 0.0;
		( *displacementFieldOld )[ 6 ] = 0.6;
		( *displacementFieldOld )[ 7 ] = 0.6;
		( *displacementFieldOld )[ 8 ] = 0.0;
		( *displacementFieldOld )[ 9 ] = 0.0;
		( *displacementFieldOld )[ 10 ] = 0.4;
		( *displacementFieldOld )[ 11 ] = 0.4;

		this->displacementField = VectorPtr( new Vector( 12, 0.0 ) );
		( *displacementField )[ 0 ] = 0.2;
		( *displacementField )[ 1 ] = 0.2;
		( *displacementField )[ 2 ] = 0.1;
		( *displacementField )[ 3 ] = 0.1;
		( *displacementField )[ 4 ] = 0.0;
		( *displacementField )[ 5 ] = 0.0;
		( *displacementField )[ 6 ] = 0.4;
		( *displacementField )[ 7 ] = 0.4;
		( *displacementField )[ 8 ] = 0.0;
		( *displacementField )[ 9 ] = 0.0;
		( *displacementField )[ 10 ] = 0.2;
		( *displacementField )[ 11 ] = 0.2;

		this->pressureFieldOld = VectorPtr( new Vector( 4, 0.0 ) );

		this->pressureField = VectorPtr( new Vector( 4, 0.0 ) );
		( *pressureField )[ 0 ] = 10.0;
		( *pressureField )[ 1 ] = 10.0;
		( *pressureField )[ 2 ] = 20.0;
		( *pressureField )[ 3 ] = 20.0;
	}
};


FixtureTestSuite( GeoVariableUpdaterTestSuite, GeoVariableUpdaterFixture );

	// GENERAL ====================================================================================================
	
	TestCase( SimpleOldFieldUpdaterTest )
	{
		SimpleOldFieldUpdater updater( pressureField, pressureFieldOld );
		updater.compute();

		checkClose( ( *pressureFieldOld )[ 0 ], 10.0, 1.0e-12 );
		checkClose( ( *pressureFieldOld )[ 1 ], 10.0, 1.0e-12 );
		checkClose( ( *pressureFieldOld )[ 2 ], 20.0, 1.0e-12 );
		checkClose( ( *pressureFieldOld )[ 3 ], 20.0, 1.0e-12 );
	}


	TestCase( SimpleGeoSimultaneousFieldsUpdaterTest )
	{
		VectorPtr solution( new Vector( ( this->grid->getCells() ).size() + this->grid->getNumberOfFaces(), 0.0 ) );
		( *solution )[ 0 ] = 0.0;
		( *solution )[ 1 ] = 1.0;
		( *solution )[ 2 ] = 2.0;
		( *solution )[ 3 ] = 3.0;
		( *solution )[ 4 ] = 4.0;
		( *solution )[ 5 ] = 5.0;
		( *solution )[ 6 ] = 6.0;
		( *solution )[ 7 ] = 7.0;
		( *solution )[ 8 ] = 8.0;
		( *solution )[ 9 ] = 9.0;
		( *solution )[ 10 ] = 10.0;
		( *solution )[ 11 ] = 11.0;
		( *solution )[ 12 ] = 12.0;
		( *solution )[ 13 ] = 13.0;
		( *solution )[ 14 ] = 14.0;
		( *solution )[ 15 ] = 15.0;

		SimpleGeoSimultaneousFieldsUpdater updater( solution, pressureField, displacementField, grid );
		updater.compute();

		checkClose( ( *pressureField )[ 0 ], 0.0, 1.0e-12 );
		checkClose( ( *pressureField )[ 1 ], 1.0, 1.0e-12 );
		checkClose( ( *pressureField )[ 2 ], 2.0, 1.0e-12 );
		checkClose( ( *pressureField )[ 3 ], 3.0, 1.0e-12 );
		checkClose( ( *displacementField )[ 0 ], 4.0, 1.0e-12 );
		checkClose( ( *displacementField )[ 1 ], 5.0, 1.0e-12 );
		checkClose( ( *displacementField )[ 2 ], 6.0, 1.0e-12 );
		checkClose( ( *displacementField )[ 3 ], 7.0, 1.0e-12 );
		checkClose( ( *displacementField )[ 4 ], 8.0, 1.0e-12 );
		checkClose( ( *displacementField )[ 5 ], 9.0, 1.0e-12 );
		checkClose( ( *displacementField )[ 6 ], 10.0, 1.0e-12 );
		checkClose( ( *displacementField )[ 7 ], 11.0, 1.0e-12 );
		checkClose( ( *displacementField )[ 8 ], 12.0, 1.0e-12 );
		checkClose( ( *displacementField )[ 9 ], 13.0, 1.0e-12 );
		checkClose( ( *displacementField )[ 10 ], 14.0, 1.0e-12 );
		checkClose( ( *displacementField )[ 11 ], 15.0, 1.0e-12 );
	}


	TestCase( OldNewFieldDiffRelaxationUpdaterTest )
	{
		( *pressureFieldOld )[ 0 ] = 0;
		( *pressureFieldOld )[ 1 ] = 1;
		( *pressureFieldOld )[ 2 ] = 2;
		( *pressureFieldOld )[ 3 ] = 3;

		double relaxationCoeff = 0.8;
		OldNewFieldDiffRelaxationUpdater updater( pressureField, pressureFieldOld, relaxationCoeff );
		updater.compute();

		checkClose( ( *pressureField )[ 0 ], 8.0, 1.0e-12 );
		checkClose( ( *pressureField )[ 1 ], 8.2, 1.0e-12 );
		checkClose( ( *pressureField )[ 2 ], 16.4, 1.0e-12 );
		checkClose( ( *pressureField )[ 3 ], 16.6, 1.0e-12 );
	}


	TestCase( DeformationUpdaterTest )
	{
		VectorPtr horizontalDeformations( new Vector( ( grid->getCells() ).size(), 0.0 ) );
		VectorPtr verticalDeformations( new Vector( ( grid->getCells() ).size(), 0.0 ) );
		VectorPtr crossDeformations( new Vector( ( grid->getCells() ).size(), 0.0 ) );

		DeformationUpdater updater( horizontalDeformations, verticalDeformations, crossDeformations, displacementField, grid );
		updater.compute();

		checkClose( ( *horizontalDeformations )[ 0 ], 0.2, 1.0e-12 );
		checkClose( ( *horizontalDeformations )[ 1 ], 0.2, 1.0e-12 );
		checkClose( ( *horizontalDeformations )[ 2 ], 0.2, 1.0e-12 );
		checkClose( ( *horizontalDeformations )[ 3 ], 0.2, 1.0e-12 );

		checkClose( ( *verticalDeformations )[ 0 ], 0.1, 1.0e-12 );
		checkClose( ( *verticalDeformations )[ 1 ], 0.1, 1.0e-12 );
		checkClose( ( *verticalDeformations )[ 2 ], 0.1, 1.0e-12 );
		checkClose( ( *verticalDeformations )[ 3 ], 0.1, 1.0e-12 );

		checkClose( ( *crossDeformations )[ 0 ], 0.0, 1.0e-12 );
		checkClose( ( *crossDeformations )[ 1 ], 0.0, 1.0e-12 );
		checkClose( ( *crossDeformations )[ 2 ], 0.0, 1.0e-12 );
		checkClose( ( *crossDeformations )[ 3 ], 0.0, 1.0e-12 );
	}


	// POROSITY ===================================================================================================

	TestCase( GeoSegregatedPoroelasticPorosityUpdaterTest )
	{
		VectorPtr porosityOld( new Vector( 4, 0.1 ) );
		VectorPtr porosity( new Vector( 4, 0.0 ) );
		FieldInterpolatorPtr BiotCoefficientInterpolator( new ConstantInterpolator( 0.6 ) );
		FieldInterpolatorPtr solidPhaseCompressibilityInterpolator( new ConstantInterpolator( 0.05 ) );

		GeoSegregatedPoroelasticPorosityUpdater updater( porosity, porosityOld, displacementField, displacementFieldOld, pressureField, pressureFieldOld,
			BiotCoefficientInterpolator, solidPhaseCompressibilityInterpolator, grid );
		updater.compute();

		checkClose( ( *porosity )[ 0 ], 0.25, 1.0e-12 );
		checkClose( ( *porosity )[ 1 ], 0.25, 1.0e-12 );
		checkClose( ( *porosity )[ 2 ], 0.5, 1.0e-12 );
		checkClose( ( *porosity )[ 3 ], 0.5, 1.0e-12 );
	}


	TestCase( GeoSimultaneousPoroelasticPorosityUpdaterTest )
	{
		VectorPtr porosityOld( new Vector( 4, 0.1 ) );
		VectorPtr porosity( new Vector( 4, 0.0 ) );
		FieldInterpolatorPtr BiotCoefficientInterpolator( new ConstantInterpolator( 0.6 ) );
		FieldInterpolatorPtr solidPhaseCompressibilityInterpolator( new ConstantInterpolator( 0.05 ) );

		VectorPtr solutionOld( new Vector( ( grid->getCells() ).size() + grid->getNumberOfFaces(), 0.0 ) );
		( *solutionOld )[ 0 ] = 0.0;
		( *solutionOld )[ 1 ] = 0.0;
		( *solutionOld )[ 2 ] = 0.0;
		( *solutionOld )[ 3 ] = 0.0;
		( *solutionOld )[ 4 ] = 0.3;
		( *solutionOld )[ 5 ] = 0.3;
		( *solutionOld )[ 6 ] = 0.2;
		( *solutionOld )[ 7 ] = 0.2;
		( *solutionOld )[ 8 ] = 0.0;
		( *solutionOld )[ 9 ] = 0.0;
		( *solutionOld )[ 10 ] = 0.6;
		( *solutionOld )[ 11 ] = 0.6;
		( *solutionOld )[ 12 ] = 0.0;
		( *solutionOld )[ 13 ] = 0.0;
		( *solutionOld )[ 14 ] = 0.4;
		( *solutionOld )[ 15 ] = 0.4;

		VectorPtr solution( new Vector( ( grid->getCells() ).size() + grid->getNumberOfFaces(), 0.0 ) );
		( *solution )[ 0 ] = 10.0;
		( *solution )[ 1 ] = 10.0;
		( *solution )[ 2 ] = 20.0;
		( *solution )[ 3 ] = 20.0;
		( *solution )[ 4 ] = 0.2;
		( *solution )[ 5 ] = 0.2;
		( *solution )[ 6 ] = 0.1;
		( *solution )[ 7 ] = 0.1;
		( *solution )[ 8 ] = 0.0;
		( *solution )[ 9 ] = 0.0;
		( *solution )[ 10 ] = 0.4;
		( *solution )[ 11 ] = 0.4;
		( *solution )[ 12 ] = 0.0;
		( *solution )[ 13 ] = 0.0;
		( *solution )[ 14 ] = 0.2;
		( *solution )[ 15 ] = 0.2;

		GeoSimultaneousPoroelasticPorosityUpdater updater( porosity, porosityOld, solution, solutionOld, BiotCoefficientInterpolator, solidPhaseCompressibilityInterpolator, grid);
		updater.compute();

		checkClose( ( *porosity )[ 0 ], 0.25, 1.0e-12 );
		checkClose( ( *porosity )[ 1 ], 0.25, 1.0e-12 );
		checkClose( ( *porosity )[ 2 ], 0.5, 1.0e-12 );
		checkClose( ( *porosity )[ 3 ], 0.5, 1.0e-12 );
	}


	// PERMEABILITY ===============================================================================================


TestSuiteEnd();


// ================================================================================================================
// ================================================================================================================
// ================================================================================================================


struct GeoVariableUpdaterFixture_2
{
	GridPtr grid;
	VectorPtr displacementField;
	VectorPtr pressureField;
	
	GeoVariableUpdaterFixture_2()
	{
		CartesianGridBuilderPtr builder( new CartesianGridBuilder() );
		int nx = 2;
		int ny = 2;
		double dx = 1;
		double dy = 1;
		this->grid = builder->build( nx, dx, ny, dy );

		/////////////////////////////////////////// Displacement
		//   -------0.4--------------0.8------   // Field
		//   |               |               |   //
		//   |               |               |   //
		//   0      20      0.3      20     0.4  //
		//   |               |               |   //
		//   |               |               |   //
		//   -------0.2--------------0.6------   //
		//   |               |               |   //
		//   |               |               |   //
		//   0      10      0.1      10     0.2  //
		//   |               |               |   //
		//   |               |               |   //
		//   --------0----------------0-------   //
		///////////////////////////////////////////

		this->displacementField = VectorPtr( new Vector( 12, 0.0 ) );
		( *this->displacementField )[ 0 ] = 0.1;
		( *this->displacementField )[ 1 ] = 0.3;
		( *this->displacementField )[ 2 ] = 0.2;
		( *this->displacementField )[ 3 ] = 0.6;
		( *this->displacementField )[ 4 ] = 0.0;
		( *this->displacementField )[ 5 ] = 0.0;
		( *this->displacementField )[ 6 ] = 0.2;
		( *this->displacementField )[ 7 ] = 0.4;
		( *this->displacementField )[ 8 ] = 0.0;
		( *this->displacementField )[ 9 ] = 0.0;
		( *this->displacementField )[ 10 ] = 0.4;
		( *this->displacementField )[ 11 ] = 0.8;

		this->pressureField = VectorPtr( new Vector( 4, 0.0 ) );
		( *this->pressureField )[ 0 ] = 10.0;
		( *this->pressureField )[ 1 ] = 10.0;
		( *this->pressureField )[ 2 ] = 20.0;
		( *this->pressureField )[ 3 ] = 20.0;
	}
};


FixtureTestSuite( GeoVariableUpdaterTestSuite_2, GeoVariableUpdaterFixture_2 );

// GENERAL ====================================================================================================

TestCase( DeformationUpdaterTest )
{
	VectorPtr horizontalDeformations( new Vector( ( grid->getCells() ).size(), 0.0 ) );
	VectorPtr verticalDeformations( new Vector( ( grid->getCells() ).size(), 0.0 ) );
	VectorPtr crossDeformations( new Vector( ( grid->getCells() ).size(), 0.0 ) );

	DeformationUpdater updater( horizontalDeformations, verticalDeformations, crossDeformations, displacementField, grid );
	updater.compute();

	checkClose( ( *horizontalDeformations )[ 0 ], 0.1, 1.0e-12 );
	checkClose( ( *horizontalDeformations )[ 1 ], 0.1, 1.0e-12 );
	checkClose( ( *horizontalDeformations )[ 2 ], 0.3, 1.0e-12 );
	checkClose( ( *horizontalDeformations )[ 3 ], 0.1, 1.0e-12 );

	checkClose( ( *verticalDeformations )[ 0 ], 0.2, 1.0e-12 );
	checkClose( ( *verticalDeformations )[ 1 ], 0.6, 1.0e-12 );
	checkClose( ( *verticalDeformations )[ 2 ], 0.2, 1.0e-12 );
	checkClose( ( *verticalDeformations )[ 3 ], 0.2, 1.0e-12 );

	checkClose( ( *crossDeformations )[ 0 ], 0.15, 1.0e-12 );
	checkClose( ( *crossDeformations )[ 1 ], 0.2, 1.0e-12 );
	checkClose( ( *crossDeformations )[ 2 ], 0.25, 1.0e-12 );
	checkClose( ( *crossDeformations )[ 3 ], 0.3, 1.0e-12 );
}


TestCase( StressUpdaterTest )
{
	FieldInterpolatorPtr BiotCoefficientInterpolator( new ConstantInterpolator( 0.8 ) );
	FieldInterpolatorPtr PoissonsRatioInterpolator( new ConstantInterpolator( 0.2 ) );
	FieldInterpolatorPtr LamesParameterInterpolator( new ConstantInterpolator( 2.0 ) );
	FieldInterpolatorPtr shearModulusInterpolator( new ConstantInterpolator( 3.0 ) );

	VectorPtr horizontalDeformations( new Vector( ( grid->getCells() ).size(), 0.0 ) );
	VectorPtr verticalDeformations( new Vector( ( grid->getCells() ).size(), 0.0 ) );
	VectorPtr crossDeformations( new Vector( ( grid->getCells() ).size(), 0.0 ) );

	DeformationUpdater defUpdater( horizontalDeformations, verticalDeformations, crossDeformations, displacementField, grid );
	defUpdater.compute();

	VectorPtr horizontalStressField( new Vector( ( grid->getCells() ).size(), 0.0 ) );
	VectorPtr verticalStressField( new Vector( ( grid->getCells() ).size(), 0.0 ) );
	VectorPtr shearStressField( new Vector( ( grid->getCells() ).size(), 0.0 ) );

	StressUpdater stUpdater( horizontalStressField, verticalStressField, shearStressField, horizontalDeformations, verticalDeformations, crossDeformations,
		pressureField, BiotCoefficientInterpolator, PoissonsRatioInterpolator, LamesParameterInterpolator, shearModulusInterpolator, grid );
	stUpdater.compute();

	checkClose( ( *horizontalStressField )[ 0 ], -6.8, 1.0e-12 );
	checkClose( ( *horizontalStressField )[ 1 ], -6.0, 1.0e-12 );
	checkClose( ( *horizontalStressField )[ 2 ], -13.2, 1.0e-12 );
	checkClose( ( *horizontalStressField )[ 3 ], -14.8, 1.0e-12 );

	checkClose( ( *verticalStressField )[ 0 ], -6.2, 1.0e-12 );
	checkClose( ( *verticalStressField )[ 1 ], -3.0, 1.0e-12 );
	checkClose( ( *verticalStressField )[ 2 ], -13.8, 1.0e-12 );
	checkClose( ( *verticalStressField )[ 3 ], -14.2, 1.0e-12 );

	checkClose( ( *shearStressField )[ 0 ], 0.9, 1.0e-12 );
	checkClose( ( *shearStressField )[ 1 ], 1.2, 1.0e-12 );
	checkClose( ( *shearStressField )[ 2 ], 1.5, 1.0e-12 );
	checkClose( ( *shearStressField )[ 3 ], 1.8, 1.0e-12 );
}


TestCase( WestNormalStressMandelsProblemUpdaterTest )
{
	FieldInterpolatorPtr BiotCoefficientInterpolator( new ConstantInterpolator( 0.8 ) );
	FieldInterpolatorPtr PoissonsRatioInterpolator( new ConstantInterpolator( 0.2 ) );
	FieldInterpolatorPtr LamesParameterInterpolator( new ConstantInterpolator( 2.0 ) );
	FieldInterpolatorPtr shearModulusInterpolator( new ConstantInterpolator( 3.0 ) );

	VectorPtr horizontalDeformations( new Vector( ( grid->getCells() ).size(), 0.0 ) );
	VectorPtr verticalDeformations( new Vector( ( grid->getCells() ).size(), 0.0 ) );
	VectorPtr crossDeformations( new Vector( ( grid->getCells() ).size(), 0.0 ) );

	DeformationUpdater defUpdater( horizontalDeformations, verticalDeformations, crossDeformations, displacementField, grid );
	defUpdater.compute();

	VectorPtr horizontalStressField( new Vector( ( grid->getCells() ).size(), 0.0 ) );
	VectorPtr verticalStressField( new Vector( ( grid->getCells() ).size(), 0.0 ) );
	VectorPtr shearStressField( new Vector( ( grid->getCells() ).size(), 0.0 ) );

	StressUpdater stUpdater( horizontalStressField, verticalStressField, shearStressField, horizontalDeformations, verticalDeformations, crossDeformations,
		pressureField, BiotCoefficientInterpolator, PoissonsRatioInterpolator, LamesParameterInterpolator, shearModulusInterpolator, grid );
	stUpdater.compute();

	double forceWest = 100.0;
	double normalStressWest = 0.0;

	NormalStressMandelsProblemUpdater westNSMPUpdater( forceWest, normalStressWest, horizontalStressField, verticalStressField, ( grid->getBoundaries() )[ 0 ] );
	westNSMPUpdater.compute();

	double forceNorth = -50.0;
	double normalStressNorth = 0.0;

	NormalStressMandelsProblemUpdater northNSMPUpdater( forceNorth, normalStressNorth, horizontalStressField, verticalStressField, ( grid->getBoundaries() )[ 3 ] );
	northNSMPUpdater.compute();

	checkClose( normalStressWest, 60.0, 1.0e-12 );
	checkClose( normalStressNorth, -11.0, 1.0e-12 );
}

TestSuiteEnd();