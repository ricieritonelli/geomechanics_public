project "Tests-GeomechanicsVariableUpdater"

kind "ConsoleApp"
targetdir "../tests"

includedirs{
	"./"
}

links {
	"Lib-VariableComputer",
	"Lib-CartesianGrid",
	"Lib-Utils",
	"Lib-FieldInterpolator",
	"Ext-ACMLib"
}

files {
	"**.cpp",
	"**.h"
}

configuration "Release"
	flags{ "Unicode" }
configuration "Debug"
	flags{ "Unicode" }
