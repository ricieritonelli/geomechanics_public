#ifndef LINEAR_SYSTEM_ADDER_H
#define LINEAR_SYSTEM_ADDER_H

#include "../Config.h"

class LinearSystemAdder{
public:
	LinearSystemAdder(){}

	virtual void add() = 0;

	virtual ~LinearSystemAdder(){}
};//class LinearSystemAdder

typedef SharedPointer< LinearSystemAdder > LinearSystemAdderPtr;
typedef vector< LinearSystemAdderPtr > LinearSystemAdderArray; 

#endif