#ifndef GEO_SEGREGATED_VERT_DISPLACEMENT_GRAVITY_ADDER_H
#define GEO_SEGREGATED_VERT_DISPLACEMENT_GRAVITY_ADDER_H

#include "../../../../LinearSystemAdder.h"

// CLASS DEFINITION ==============================================================================================
class GeoSegregatedVertDisplacementGravityAdder : public LinearSystemAdder
{
protected:

	// Data Members ----------------------------------------------------------------------------------------------
	VectorPtr independent;
	FieldInterpolatorPtr fluidDensityInterpolator;
	FieldInterpolatorPtr solidDensityInterpolator;
	FieldInterpolatorPtr porosityInterpolator;
	double gravityAcceleration;
	GridPtr grid;

public:

	// Constructor and Destructor --------------------------------------------------------------------------------
	GeoSegregatedVertDisplacementGravityAdder( VectorPtr independent, FieldInterpolatorPtr fluidDensityInterpolator, FieldInterpolatorPtr solidDensityInterpolator,
		FieldInterpolatorPtr porosityInterpolator, const double& gravityAcceleration, GridPtr grid );
	virtual ~GeoSegregatedVertDisplacementGravityAdder(){}

	// Class Interface -------------------------------------------------------------------------------------------
	void add();

private:

	// Private Member Functions ----------------------------------------------------------------------------------
	double computeInternalCellCoefficient( InternalFacePtr face );
	double computeBorderCellCoefficient( ExternalFacePtr face );
	void addCellContribution( FacePtr face, const double& coefficient );

}; // class GeoSegregatedVertDisplacementGravityAdder

typedef SharedPointer< GeoSegregatedVertDisplacementGravityAdder > GeoSegregatedVertDisplacementGravityAdderPtr;

#endif