#include "GeoSegregatedVertDisplacementGravityAdder.h"

// Constructor ---------------------------------------------------------------------------------------------------
GeoSegregatedVertDisplacementGravityAdder::GeoSegregatedVertDisplacementGravityAdder( VectorPtr independent, FieldInterpolatorPtr fluidDensityInterpolator, FieldInterpolatorPtr solidDensityInterpolator,
	FieldInterpolatorPtr porosityInterpolator, const double& gravityAcceleration, GridPtr grid ) : independent( independent ), fluidDensityInterpolator( fluidDensityInterpolator ),
	solidDensityInterpolator( solidDensityInterpolator ), porosityInterpolator( porosityInterpolator ), gravityAcceleration( gravityAcceleration ), grid( grid ){}


// Class Interface -----------------------------------------------------------------------------------------------
void GeoSegregatedVertDisplacementGravityAdder::add()
{
	for ( int i = ( this->grid->getNx() - 1 ) * this->grid->getNy(); i < ( this->grid->getNx() - 1 ) * this->grid->getNy() + this->grid->getNx() * ( this->grid->getNy() - 1 ); ++i ) /* INTERNAL CELLS */
	{
		InternalFacePtr face = ( this->grid->getInternalFaces() )[ i ];
		this->addCellContribution( face, this->computeInternalCellCoefficient( face ) );
	}

	for ( int i = this->grid->getNy() * 2; i < ( this->grid->getExternalFaces() ).size(); ++i ) /* SOUTH AND NORTH BORDER CELLS */
	{
		ExternalFacePtr face = ( this->grid->getExternalFaces() )[ i ];
		this->addCellContribution( face, this->computeBorderCellCoefficient( face ) );
	}
}


// Private Member Functions --------------------------------------------------------------------------------------
double GeoSegregatedVertDisplacementGravityAdder::computeInternalCellCoefficient( InternalFacePtr face )
{
	CellPtr cellSouth = ( this->grid->getCells() )[ face->getNode0()->getHandle() ];
	CellPtr cellNorth = ( this->grid->getCells() )[ face->getNode1()->getHandle() ];
	VertexPtr westVertex = ( face->getVertices() )[ 0 ];
	VertexPtr eastVertex = ( face->getVertices() )[ 1 ];

	double dx = eastVertex->dist( *westVertex );
	double dy = cellNorth->getCentroid()->dist( *cellSouth->getCentroid() );
	double density = ( 1 - this->porosityInterpolator->getPropertyOnFace( face ) ) * this->solidDensityInterpolator->getPropertyOnFace( face ) + this->porosityInterpolator->getPropertyOnFace( face ) * fluidDensityInterpolator->getPropertyOnFace( face );
	return density * gravityAcceleration * dx * dy;
}

double GeoSegregatedVertDisplacementGravityAdder::computeBorderCellCoefficient( ExternalFacePtr face )
{
	CellPtr cell = ( this->grid->getCells() )[ face->getTopologyNode()->getHandle() ];
	VertexPtr westVertex = ( face->getVertices() )[ 0 ];
	VertexPtr eastVertex = ( face->getVertices() )[ 1 ];

	double dx = eastVertex->dist( *westVertex );
	double dy = abs( face->getCentroid()->dist( *cell->getCentroid() ) );
	double density = ( 1 - this->porosityInterpolator->getPropertyOnFace( face ) ) * this->solidDensityInterpolator->getPropertyOnFace( face ) + this->porosityInterpolator->getPropertyOnFace( face ) * fluidDensityInterpolator->getPropertyOnFace( face );
	return density * gravityAcceleration * dx * dy;
}

void GeoSegregatedVertDisplacementGravityAdder::addCellContribution( FacePtr face, const double& coefficient )
{
	( *this->independent )[ face->getHandle() ] += coefficient;
}

