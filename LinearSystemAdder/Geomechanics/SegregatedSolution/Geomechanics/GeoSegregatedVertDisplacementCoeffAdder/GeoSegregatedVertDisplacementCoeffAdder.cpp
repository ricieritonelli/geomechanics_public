#include "GeoSegregatedVertDisplacementCoeffAdder.h"

// Constructor ---------------------------------------------------------------------------------------------------
GeoSegregatedVertDisplacementCoeffAdder::GeoSegregatedVertDisplacementCoeffAdder( SparseMatrixPtr matrix, FieldInterpolatorPtr PoissonsRatioInterpolator, FieldInterpolatorPtr sheerModulusInterpolator,
	FieldInterpolatorPtr LamesParameterInterpolator, GridPtr grid )	: matrix( matrix ), PoissonsRatioInterpolator( PoissonsRatioInterpolator ), sheerModulusInterpolator( sheerModulusInterpolator ),
	LamesParameterInterpolator( LamesParameterInterpolator ), grid( grid ){}


// Class Interface -----------------------------------------------------------------------------------------------
void GeoSegregatedVertDisplacementCoeffAdder::add()
{
	foreach( CellPtr cell, this->grid->getCells() ) /* Coefficients of the HORIZONTAL FACES */
	{
		this->addHorFaceConnectionContribution( cell, this->computeHorFaceCoefficient( cell ) ); /* An_vv, As_vv */
		this->addHorFaceConnectionCrossContribution( cell ); /* Ap_vu, Aw_vu, An_vu, Anw_vu */
	}

	for ( int i = ( this->grid->getNx() - 1 ) * this->grid->getNy(); i < ( this->grid->getNx() - 1 ) * this->grid->getNy() + this->grid->getNx() * ( this->grid->getNy() - 1 ); ++i ) /* Coefficients of the INTERNAL VERTICAL FACES */
	{
		if ( ( i - ( ( this->grid->getNx() - 1 ) * this->grid->getNy() ) ) % this->grid->getNx()  != 0 )
		{
			InternalFacePtr faceEast = ( this->grid->getInternalFaces() )[ i ];
			InternalFacePtr faceWest = ( this->grid->getInternalFaces() )[ i - 1 ];
			this->addVertFaceConnectionContribution( faceWest, faceEast, this->computeVertInternalFaceCoefficient( faceWest, faceEast ) ); /* Ae_vv, Aw_vv */
			this->addVertFaceConnectionCrossContribution( faceWest, faceEast ); /* Ap_vu, Aw_vu, An_vu, Anw_vu */
		}
	}

	for (int i = ( 2 * this->grid->getNy() ) + 1; i < ( 2 * this->grid->getNy() ) + this->grid->getNx(); ++i ) /* Coefficients of the SOUTH EXTERNAL VERTICAL FACES */
	{
		ExternalFacePtr faceEast = ( this->grid->getExternalFaces() )[ i ];
		ExternalFacePtr faceWest = ( this->grid->getExternalFaces() )[ i - 1 ];
		this->addVertFaceConnectionContribution( faceWest, faceEast, this->computeVertExternalFaceCoefficient( faceWest, faceEast ) ); /* Ae_vv, Aw_vv */
	}

	for ( int i = ( 2 * this->grid->getNy() ) + this->grid->getNx() + 1; i < ( 2 * this->grid->getNx() ) + ( 2 * this->grid->getNy() ); ++i ) /* Coefficients of the NORTH EXTERNAL VERTICAL FACES */
	{
		ExternalFacePtr faceEast = ( this->grid->getExternalFaces() )[ i ];
		ExternalFacePtr faceWest = ( this->grid->getExternalFaces() )[ i - 1 ];
		this->addVertFaceConnectionContribution( faceWest, faceEast, this->computeVertExternalFaceCoefficient( faceWest, faceEast ) ); /* Ae_vv, Aw_vv */
	}
}


// Private Member Functions --------------------------------------------------------------------------------------
double GeoSegregatedVertDisplacementCoeffAdder::computeHorFaceCoefficient( CellPtr cell )
{
	FacePtr faceEast = cell->getEastFaceID();
	FacePtr faceWest = cell->getWestFaceID();
	FacePtr faceNorth = cell->getNorthFaceID();
	FacePtr faceSouth = cell->getSouthFaceID();
	double dx = faceEast->getCentroid()->dist( *faceWest->getCentroid() );
	double dy = faceNorth->getCentroid()->dist( *faceSouth->getCentroid() );
	return this->LamesParameterInterpolator->getPropertyOnCell( cell ) * ( 1 - this->PoissonsRatioInterpolator->getPropertyOnCell( cell ) ) * dx / ( dy * this->PoissonsRatioInterpolator->getPropertyOnCell( cell ) );
}

void GeoSegregatedVertDisplacementCoeffAdder::addHorFaceConnectionContribution( CellPtr cell, const double& coefficient )
{
	( *this->matrix )( cell->getSouthFaceID()->getHandle(), cell->getSouthFaceID()->getHandle() ) += coefficient;
	( *this->matrix )( cell->getNorthFaceID()->getHandle(), cell->getNorthFaceID()->getHandle() ) += coefficient;
	( *this->matrix )( cell->getSouthFaceID()->getHandle(), cell->getNorthFaceID()->getHandle() ) -= coefficient;
	( *this->matrix )( cell->getNorthFaceID()->getHandle(), cell->getSouthFaceID()->getHandle() ) -= coefficient;
}

void GeoSegregatedVertDisplacementCoeffAdder::addHorFaceConnectionCrossContribution( CellPtr cell )
{
	( *this->matrix )( cell->getSouthFaceID()->getHandle(), cell->getWestFaceID()->getHandle() ) += this->LamesParameterInterpolator->getPropertyOnCell( cell );
	( *this->matrix )( cell->getSouthFaceID()->getHandle(), cell->getEastFaceID()->getHandle() ) -= this->LamesParameterInterpolator->getPropertyOnCell( cell );
	( *this->matrix )( cell->getNorthFaceID()->getHandle(), cell->getWestFaceID()->getHandle() ) -= this->LamesParameterInterpolator->getPropertyOnCell( cell );
	( *this->matrix )( cell->getNorthFaceID()->getHandle(), cell->getEastFaceID()->getHandle() ) += this->LamesParameterInterpolator->getPropertyOnCell( cell );

// 	cout << "( " << cell->getSouthFaceID()->getHandle() << ", " << cell->getWestFaceID()->getHandle() << " ) = " << ( *this->matrix )( cell->getSouthFaceID()->getHandle(), cell->getWestFaceID()->getHandle() ) << endl;
// 	cout << "( " << cell->getSouthFaceID()->getHandle() << ", " << cell->getEastFaceID()->getHandle() << " ) = " << ( *this->matrix )( cell->getSouthFaceID()->getHandle(), cell->getEastFaceID()->getHandle() ) << endl;
// 	cout << "( " << cell->getNorthFaceID()->getHandle() << ", " << cell->getWestFaceID()->getHandle() << " ) = " << ( *this->matrix )( cell->getNorthFaceID()->getHandle(), cell->getWestFaceID()->getHandle() ) << endl;
// 	cout << "( " << cell->getNorthFaceID()->getHandle() << ", " << cell->getEastFaceID()->getHandle() << " ) = " << ( *this->matrix )( cell->getNorthFaceID()->getHandle(), cell->getEastFaceID()->getHandle() ) << endl;
// 	cout << endl;
}

double GeoSegregatedVertDisplacementCoeffAdder::computeVertInternalFaceCoefficient( InternalFacePtr faceWest, InternalFacePtr faceEast )
{
	VertexPtr vertex = ( faceWest->getVertices() )[ 1 ];
	CellPtr cellSouthWest = ( this->grid->getCells() )[ faceWest->getNode0()->getHandle() ];
	CellPtr cellNorthWest = ( this->grid->getCells() )[ faceWest->getNode1()->getHandle() ];

	double dx = faceEast->getCentroid()->dist( *faceWest->getCentroid() );
	double dy = cellNorthWest->getEastFaceID()->getCentroid()->dist( *vertex ) + vertex->dist( *cellSouthWest->getEastFaceID()->getCentroid() );
	return this->sheerModulusInterpolator->getPropertyOnVertex( vertex ) * dy / dx;
}

double GeoSegregatedVertDisplacementCoeffAdder::computeVertExternalFaceCoefficient( ExternalFacePtr faceWest, ExternalFacePtr faceEast )
{
	VertexPtr vertex = ( faceWest->getVertices() )[ 1 ];
	CellPtr cellWest = ( this->grid->getCells() )[ faceWest->getTopologyNode()->getHandle() ];

	double dx = faceEast->getCentroid()->dist( *faceWest->getCentroid() );
	double dy = abs( cellWest->getEastFaceID()->getCentroid()->dist( *vertex ) );
	return this->sheerModulusInterpolator->getPropertyOnVertex( vertex ) * dy / dx;
}

void GeoSegregatedVertDisplacementCoeffAdder::addVertFaceConnectionContribution( FacePtr faceWest, FacePtr faceEast, const double& coefficient )
{
	( *this->matrix )( faceWest->getHandle(), faceWest->getHandle() ) += coefficient;
	( *this->matrix )( faceEast->getHandle(), faceEast->getHandle() ) += coefficient;
	( *this->matrix )( faceWest->getHandle(), faceEast->getHandle() ) -= coefficient;
	( *this->matrix )( faceEast->getHandle(), faceWest->getHandle() ) -= coefficient;
}

void GeoSegregatedVertDisplacementCoeffAdder::addVertFaceConnectionCrossContribution( InternalFacePtr faceWest, InternalFacePtr faceEast )
{
	VertexPtr vertex = ( faceWest->getVertices() )[ 1 ];
	CellPtr cellSouthWest = ( this->grid->getCells() )[ faceWest->getNode0()->getHandle() ];
	CellPtr cellNorthWest = ( this->grid->getCells() )[ faceWest->getNode1()->getHandle() ];

	( *this->matrix )( faceWest->getHandle(), cellSouthWest->getEastFaceID()->getHandle() ) += this->sheerModulusInterpolator->getPropertyOnVertex( vertex );
	( *this->matrix )( faceWest->getHandle(), cellNorthWest->getEastFaceID()->getHandle() ) -= this->sheerModulusInterpolator->getPropertyOnVertex( vertex );
	( *this->matrix )( faceEast->getHandle(), cellSouthWest->getEastFaceID()->getHandle() ) -= this->sheerModulusInterpolator->getPropertyOnVertex( vertex );
	( *this->matrix )( faceEast->getHandle(), cellNorthWest->getEastFaceID()->getHandle() ) += this->sheerModulusInterpolator->getPropertyOnVertex( vertex );

// 	cout << "( " << faceWest->getHandle() << ", " << cellSouthWest->getEastFaceID()->getHandle() << " ) = " << ( *this->matrix )( faceWest->getHandle(), cellSouthWest->getEastFaceID()->getHandle() ) << endl;
// 	cout << "( " << faceWest->getHandle() << ", " << cellNorthWest->getEastFaceID()->getHandle() << " ) = " << ( *this->matrix )( faceWest->getHandle(), cellNorthWest->getEastFaceID()->getHandle() ) << endl;
// 	cout << "( " << faceEast->getHandle() << ", " << cellSouthWest->getEastFaceID()->getHandle() << " ) = " << ( *this->matrix )( faceEast->getHandle(), cellSouthWest->getEastFaceID()->getHandle() ) << endl;
// 	cout << "( " << faceEast->getHandle() << ", " << cellNorthWest->getEastFaceID()->getHandle() << " ) = " << ( *this->matrix )( faceEast->getHandle(), cellNorthWest->getEastFaceID()->getHandle() ) << endl;
// 	cout << endl;
}
