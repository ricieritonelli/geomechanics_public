#ifndef GEO_SEGREGATED_VERT_DISPLACEMENT_COEFF_ADDER_H
#define GEO_SEGREGATED_VERT_DISPLACEMENT_COEFF_ADDER_H

#include "../../../../LinearSystemAdder.h"

// CLASS DEFINITION ==============================================================================================
class GeoSegregatedVertDisplacementCoeffAdder : public LinearSystemAdder
{
protected:

	// Data Members ----------------------------------------------------------------------------------------------
	SparseMatrixPtr matrix;
	FieldInterpolatorPtr PoissonsRatioInterpolator;
	FieldInterpolatorPtr sheerModulusInterpolator;
	FieldInterpolatorPtr LamesParameterInterpolator;
	GridPtr grid;

public:

	// Constructor and Destructor --------------------------------------------------------------------------------
	GeoSegregatedVertDisplacementCoeffAdder( SparseMatrixPtr matrix, FieldInterpolatorPtr PoissonsRatioInterpolator, FieldInterpolatorPtr sheerModulusInterpolator,
		FieldInterpolatorPtr LamesParameterInterpolator, GridPtr grid );
	virtual ~GeoSegregatedVertDisplacementCoeffAdder(){}

	// Class Interface -------------------------------------------------------------------------------------------
	void add();

private:

	// Private Member Functions ----------------------------------------------------------------------------------
	double computeHorFaceCoefficient( CellPtr cell );
	void addHorFaceConnectionContribution( CellPtr cell, const double& coefficient );
	void addHorFaceConnectionCrossContribution( CellPtr cell );
	double computeVertInternalFaceCoefficient( InternalFacePtr faceWest, InternalFacePtr faceEast );
	double computeVertExternalFaceCoefficient( ExternalFacePtr faceWest, ExternalFacePtr faceEast );
	void addVertFaceConnectionContribution( FacePtr faceWest, FacePtr faceEast, const double& coefficient );
	void addVertFaceConnectionCrossContribution( InternalFacePtr faceWest, InternalFacePtr faceEast );
	
}; // class GeoSegregatedVertDisplacementCoeffAdder

typedef SharedPointer< GeoSegregatedVertDisplacementCoeffAdder > GeoSegregatedVertDisplacementCoeffAdderPtr;

#endif