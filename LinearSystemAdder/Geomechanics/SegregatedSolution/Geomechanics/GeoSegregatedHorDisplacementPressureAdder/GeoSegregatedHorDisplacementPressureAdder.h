#ifndef GEO_SEGREGATED_HOR_DISPLACEMENT_PRESSURE_ADDER_H
#define GEO_SEGREGATED_HOR_DISPLACEMENT_PRESSURE_ADDER_H

#include "../../../../LinearSystemAdder.h"

// CLASS DEFINITION ==============================================================================================
class GeoSegregatedHorDisplacementPressureAdder : public LinearSystemAdder
{
protected:

	// Data Members ----------------------------------------------------------------------------------------------
	VectorPtr independent;
	VectorPtr pressureField;
	FieldInterpolatorPtr BiotCoefficientInterpolator;
	GridPtr grid;

public:

	// Constructor and Destructor --------------------------------------------------------------------------------
	GeoSegregatedHorDisplacementPressureAdder( VectorPtr independent, VectorPtr pressureField, FieldInterpolatorPtr BiotCoefficientInterpolator, GridPtr grid );
	virtual ~GeoSegregatedHorDisplacementPressureAdder(){}

	// Class Interface -------------------------------------------------------------------------------------------
	void add();

private:

	// Private Member Functions ----------------------------------------------------------------------------------
	double computeFaceCoefficient( CellPtr cell );
	void addFaceConnectionContribution( CellPtr cell, const double& coefficient );

}; // class GeoSegregatedHorDisplacementPressureAdder

typedef SharedPointer< GeoSegregatedHorDisplacementPressureAdder > GeoSegregatedHorDisplacementPressureAdderPtr;

#endif