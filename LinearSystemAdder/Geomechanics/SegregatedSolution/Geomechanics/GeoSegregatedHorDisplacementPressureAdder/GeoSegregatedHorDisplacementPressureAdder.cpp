#include "GeoSegregatedHorDisplacementPressureAdder.h"

// Constructor ---------------------------------------------------------------------------------------------------
GeoSegregatedHorDisplacementPressureAdder::GeoSegregatedHorDisplacementPressureAdder( VectorPtr independent, VectorPtr pressureField, FieldInterpolatorPtr BiotCoefficientInterpolator, GridPtr grid ) : 
	independent( independent ), pressureField( pressureField ), BiotCoefficientInterpolator( BiotCoefficientInterpolator ), grid( grid ){}


// Class Interface -----------------------------------------------------------------------------------------------
void GeoSegregatedHorDisplacementPressureAdder::add()
{
	foreach( CellPtr cell, this->grid->getCells() )
	{
		this->addFaceConnectionContribution( cell, this->computeFaceCoefficient( cell ) );
	}
}


// Private Member Functions --------------------------------------------------------------------------------------
double GeoSegregatedHorDisplacementPressureAdder::computeFaceCoefficient( CellPtr cell )
{
	double dy = cell->getNorthFaceID()->getCentroid()->dist( *cell->getSouthFaceID()->getCentroid() );
	return this->BiotCoefficientInterpolator->getPropertyOnCell( cell ) * ( *this->pressureField )[ cell->getHandle() ] * dy;
}

void GeoSegregatedHorDisplacementPressureAdder::addFaceConnectionContribution( CellPtr cell, const double& coefficient )
{
	( *this->independent )[ cell->getWestFaceID()->getHandle() ] -= coefficient;
	( *this->independent )[ cell->getEastFaceID()->getHandle() ] += coefficient;
}

