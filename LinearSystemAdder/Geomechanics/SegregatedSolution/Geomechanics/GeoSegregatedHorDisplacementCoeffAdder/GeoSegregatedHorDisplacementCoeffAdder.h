#ifndef GEO_SEGREGATED_HOR_DISPLACEMENT_COEFF_ADDER_H
#define GEO_SEGREGATED_HOR_DISPLACEMENT_COEFF_ADDER_H

#include "../../../../LinearSystemAdder.h"

// CLASS DEFINITION ==============================================================================================
class GeoSegregatedHorDisplacementCoeffAdder : public LinearSystemAdder
{
protected:

	// Data Members ----------------------------------------------------------------------------------------------
	SparseMatrixPtr matrix;
	FieldInterpolatorPtr PoissonsRatioInterpolator;
	FieldInterpolatorPtr sheerModulusInterpolator;
	FieldInterpolatorPtr LamesParameterInterpolator;
	GridPtr grid;

public:

	// Constructor and Destructor --------------------------------------------------------------------------------
	GeoSegregatedHorDisplacementCoeffAdder( SparseMatrixPtr matrix, FieldInterpolatorPtr PoissonsRatioInterpolator, FieldInterpolatorPtr sheerModulusInterpolator,
		FieldInterpolatorPtr LamesParameterInterpolator, GridPtr grid );
	virtual ~GeoSegregatedHorDisplacementCoeffAdder(){}

	// Class Interface -------------------------------------------------------------------------------------------
	void add();

private:

	// Private Member Functions ----------------------------------------------------------------------------------
	double computeVertFaceCoefficient( CellPtr cell );
	void addVertFaceConnectionContribution( CellPtr cell, const double& coefficient );
	void addVertFaceConnectionCrossContribution( CellPtr cell );
	double computeHorInternalFaceCoefficient( InternalFacePtr faceSouth, InternalFacePtr faceNorth );
	double computeHorExternalFaceCoefficient( ExternalFacePtr faceSouth, ExternalFacePtr faceNorth );
	void addHorFaceConnectionContribution( FacePtr faceSouth, FacePtr faceNorth, const double& coefficient );
	void addHorFaceConnectionCrossContribution( InternalFacePtr faceSouth, InternalFacePtr faceNorth );

}; // class GeoSegregatedHorDisplacementCoeffAdder

typedef SharedPointer< GeoSegregatedHorDisplacementCoeffAdder > GeoSegregatedHorDisplacementCoeffAdderPtr;

#endif