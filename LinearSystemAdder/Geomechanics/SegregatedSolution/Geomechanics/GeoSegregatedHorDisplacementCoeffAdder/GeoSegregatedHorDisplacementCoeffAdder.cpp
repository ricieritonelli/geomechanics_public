#include "GeoSegregatedHorDisplacementCoeffAdder.h"

// Constructor ---------------------------------------------------------------------------------------------------
GeoSegregatedHorDisplacementCoeffAdder::GeoSegregatedHorDisplacementCoeffAdder( SparseMatrixPtr matrix, FieldInterpolatorPtr PoissonsRatioInterpolator, FieldInterpolatorPtr sheerModulusInterpolator,
	FieldInterpolatorPtr LamesParameterInterpolator, GridPtr grid )	: matrix( matrix ), PoissonsRatioInterpolator( PoissonsRatioInterpolator ), sheerModulusInterpolator( sheerModulusInterpolator ),
	LamesParameterInterpolator( LamesParameterInterpolator ), grid( grid ){}


// Class Interface -----------------------------------------------------------------------------------------------
void GeoSegregatedHorDisplacementCoeffAdder::add()
{
	foreach( CellPtr cell, this->grid->getCells() ) /* Coefficients of the VERTICAL FACES */
	{
		this->addVertFaceConnectionContribution( cell, this->computeVertFaceCoefficient( cell ) ); /* Ae_uu, Aw_uu */
		this->addVertFaceConnectionCrossContribution( cell ); /* Ap_uv, Ae_uv, As_uv, Ase_uv */
	}

	for ( int i = 0; i < ( this->grid->getNx() - 1 ) * ( this->grid->getNy() - 1 ); ++i ) /* Coefficients of the INTERNAL HORIZONTAL FACES */
	{
		InternalFacePtr faceSouth = ( this->grid->getInternalFaces() )[ i ];
		InternalFacePtr faceNorth = ( this->grid->getInternalFaces() )[ i + ( this->grid->getNx() - 1 ) ];
		this->addHorFaceConnectionContribution( faceSouth, faceNorth, this->computeHorInternalFaceCoefficient( faceSouth, faceNorth ) ); /* An_uu, As_uu */
		this->addHorFaceConnectionCrossContribution( faceSouth, faceNorth ); /* Ap_uv, Ae_uv, As_uv, Ase_uv */
	}

	for ( int i = 0; i < ( this->grid->getNy() - 1 ); ++i ) /* Coefficients of the WEST EXTERNAL HORIZONTAL FACES */
	{
		ExternalFacePtr faceSouth = ( this->grid->getExternalFaces() )[ i ];
		ExternalFacePtr faceNorth = ( this->grid->getExternalFaces() )[ i + 1 ];
		this->addHorFaceConnectionContribution( faceSouth, faceNorth, this->computeHorExternalFaceCoefficient( faceSouth, faceNorth ) ); /* An_uu, As_uu */
	}

	for (int i = ( this->grid->getNy() ); i < ( this->grid->getNy() * 2 - 1); ++i ) /* Coefficients of the EAST EXTERNAL HORIZONTAL FACES */
	{
		ExternalFacePtr faceSouth = ( this->grid->getExternalFaces() )[ i ];
		ExternalFacePtr faceNorth = ( this->grid->getExternalFaces() )[ i + 1 ];
		this->addHorFaceConnectionContribution( faceSouth, faceNorth, this->computeHorExternalFaceCoefficient( faceSouth, faceNorth ) ); /* An_uu, As_uu */
	}
}


// Private Member Functions --------------------------------------------------------------------------------------
double GeoSegregatedHorDisplacementCoeffAdder::computeVertFaceCoefficient( CellPtr cell )
{
	FacePtr faceEast = cell->getEastFaceID();
	FacePtr faceWest = cell->getWestFaceID();
	FacePtr faceNorth = cell->getNorthFaceID();
	FacePtr faceSouth = cell->getSouthFaceID();
	double dx = faceEast->getCentroid()->dist( *faceWest->getCentroid() );
	double dy = faceNorth->getCentroid()->dist( *faceSouth->getCentroid() );
	return this->LamesParameterInterpolator->getPropertyOnCell( cell ) * ( 1 - this->PoissonsRatioInterpolator->getPropertyOnCell( cell ) ) * dy / ( dx * this->PoissonsRatioInterpolator->getPropertyOnCell( cell ) );
}

void GeoSegregatedHorDisplacementCoeffAdder::addVertFaceConnectionContribution( CellPtr cell, const double& coefficient )
{
	( *this->matrix )( cell->getWestFaceID()->getHandle(), cell->getWestFaceID()->getHandle() ) += coefficient;
	( *this->matrix )( cell->getEastFaceID()->getHandle(), cell->getEastFaceID()->getHandle() ) += coefficient;
	( *this->matrix )( cell->getWestFaceID()->getHandle(), cell->getEastFaceID()->getHandle() ) -= coefficient;
	( *this->matrix )( cell->getEastFaceID()->getHandle(), cell->getWestFaceID()->getHandle() ) -= coefficient;
}

void GeoSegregatedHorDisplacementCoeffAdder::addVertFaceConnectionCrossContribution( CellPtr cell )
{
	( *this->matrix )( cell->getWestFaceID()->getHandle(), cell->getNorthFaceID()->getHandle() ) -= this->LamesParameterInterpolator->getPropertyOnCell( cell );
	( *this->matrix )( cell->getWestFaceID()->getHandle(), cell->getSouthFaceID()->getHandle() ) += this->LamesParameterInterpolator->getPropertyOnCell( cell );
	( *this->matrix )( cell->getEastFaceID()->getHandle(), cell->getNorthFaceID()->getHandle() ) += this->LamesParameterInterpolator->getPropertyOnCell( cell );
	( *this->matrix )( cell->getEastFaceID()->getHandle(), cell->getSouthFaceID()->getHandle() ) -= this->LamesParameterInterpolator->getPropertyOnCell( cell );
}

double GeoSegregatedHorDisplacementCoeffAdder::computeHorInternalFaceCoefficient( InternalFacePtr faceSouth, InternalFacePtr faceNorth )
{
	VertexPtr vertex = ( faceSouth->getVertices() )[ 1 ];
	CellPtr cellSouthWest = ( this->grid->getCells() )[ faceSouth->getNode0()->getHandle() ];
	CellPtr cellSouthEast = ( this->grid->getCells() )[ faceSouth->getNode1()->getHandle() ];

	double dy = faceNorth->getCentroid()->dist( *faceSouth->getCentroid() );
	double dx = cellSouthEast->getNorthFaceID()->getCentroid()->dist( *vertex ) + vertex->dist( *cellSouthWest->getNorthFaceID()->getCentroid() );
	return this->sheerModulusInterpolator->getPropertyOnVertex( vertex ) * dx / dy;
}

double GeoSegregatedHorDisplacementCoeffAdder::computeHorExternalFaceCoefficient( ExternalFacePtr faceSouth, ExternalFacePtr faceNorth )
{
	VertexPtr vertex = ( faceSouth->getVertices() )[ 1 ];
	CellPtr cellSouth = ( this->grid->getCells() )[ faceSouth->getTopologyNode()->getHandle() ];

	double dy = faceNorth->getCentroid()->dist( *faceSouth->getCentroid() );
	double dx = abs( cellSouth->getNorthFaceID()->getCentroid()->dist( *vertex ) );
	return this->sheerModulusInterpolator->getPropertyOnVertex( vertex ) * dx / dy;
}

void GeoSegregatedHorDisplacementCoeffAdder::addHorFaceConnectionContribution( FacePtr faceSouth, FacePtr faceNorth, const double& coefficient )
{
	( *this->matrix )( faceSouth->getHandle(), faceSouth->getHandle() ) += coefficient;
	( *this->matrix )( faceNorth->getHandle(), faceNorth->getHandle() ) += coefficient;
	( *this->matrix )( faceSouth->getHandle(), faceNorth->getHandle() ) -= coefficient;
	( *this->matrix )( faceNorth->getHandle(), faceSouth->getHandle() ) -= coefficient;
}

void GeoSegregatedHorDisplacementCoeffAdder::addHorFaceConnectionCrossContribution( InternalFacePtr faceSouth, InternalFacePtr faceNorth )
{
	VertexPtr vertex = ( faceSouth->getVertices() )[ 1 ];
	CellPtr cellSouthWest = ( this->grid->getCells() )[ faceSouth->getNode0()->getHandle() ];
	CellPtr cellSouthEast = ( this->grid->getCells() )[ faceSouth->getNode1()->getHandle() ];

	( *this->matrix )( faceSouth->getHandle(), cellSouthWest->getNorthFaceID()->getHandle() ) += this->sheerModulusInterpolator->getPropertyOnVertex( vertex );
	( *this->matrix )( faceSouth->getHandle(), cellSouthEast->getNorthFaceID()->getHandle() ) -= this->sheerModulusInterpolator->getPropertyOnVertex( vertex );
	( *this->matrix )( faceNorth->getHandle(), cellSouthWest->getNorthFaceID()->getHandle() ) -= this->sheerModulusInterpolator->getPropertyOnVertex( vertex );
	( *this->matrix )( faceNorth->getHandle(), cellSouthEast->getNorthFaceID()->getHandle() ) += this->sheerModulusInterpolator->getPropertyOnVertex( vertex );
}
