#ifndef GEO_SEGREGATED_VERT_DISPLACEMENT_PRESSURE_ADDER_H
#define GEO_SEGREGATED_VERT_DISPLACEMENT_PRESSURE_ADDER_H

#include "../../../../LinearSystemAdder.h"

// CLASS DEFINITION ==============================================================================================
class GeoSegregatedVertDisplacementPressureAdder : public LinearSystemAdder
{
protected:

	// Data Members ----------------------------------------------------------------------------------------------
	VectorPtr independent;
	VectorPtr pressureField;
	FieldInterpolatorPtr BiotCoefficientInterpolator;
	GridPtr grid;

public:

	// Constructor and Destructor --------------------------------------------------------------------------------
	GeoSegregatedVertDisplacementPressureAdder( VectorPtr independent, VectorPtr pressureField, FieldInterpolatorPtr BiotCoefficientInterpolator, GridPtr grid );
	virtual ~GeoSegregatedVertDisplacementPressureAdder(){}

	// Class Interface -------------------------------------------------------------------------------------------
	void add();

private:

	// Private Member Functions ----------------------------------------------------------------------------------
	double computeFaceCoefficient( CellPtr cell );
	void addFaceConnectionContribution( CellPtr cell, const double& coefficient );

}; // class GeoSegregatedVertDisplacementPressureAdder

typedef SharedPointer< GeoSegregatedVertDisplacementPressureAdder > GeoSegregatedVertDisplacementPressureAdderPtr;

#endif