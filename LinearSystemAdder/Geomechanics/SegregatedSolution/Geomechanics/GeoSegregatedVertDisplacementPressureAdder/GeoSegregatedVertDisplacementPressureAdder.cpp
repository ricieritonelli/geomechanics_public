#include "GeoSegregatedVertDisplacementPressureAdder.h"

// Constructor ---------------------------------------------------------------------------------------------------
GeoSegregatedVertDisplacementPressureAdder::GeoSegregatedVertDisplacementPressureAdder( VectorPtr independent, VectorPtr pressureField, FieldInterpolatorPtr BiotCoefficientInterpolator, GridPtr grid ) : 
	independent( independent ), pressureField( pressureField ), BiotCoefficientInterpolator( BiotCoefficientInterpolator ), grid( grid ){}


// Class Interface -----------------------------------------------------------------------------------------------
void GeoSegregatedVertDisplacementPressureAdder::add()
{
	foreach( CellPtr cell, this->grid->getCells() )
	{
		this->addFaceConnectionContribution( cell, this->computeFaceCoefficient( cell ) );
	}
}


// Private Member Functions --------------------------------------------------------------------------------------
double GeoSegregatedVertDisplacementPressureAdder::computeFaceCoefficient( CellPtr cell )
{
	double dx = cell->getEastFaceID()->getCentroid()->dist( *cell->getWestFaceID()->getCentroid() );
	return this->BiotCoefficientInterpolator->getPropertyOnCell( cell ) * ( *this->pressureField )[ cell->getHandle() ] * dx;
}

void GeoSegregatedVertDisplacementPressureAdder::addFaceConnectionContribution( CellPtr cell, const double& coefficient )
{
	( *this->independent )[ cell->getSouthFaceID()->getHandle() ] -= coefficient;
	( *this->independent )[ cell->getNorthFaceID()->getHandle() ] += coefficient;
}

