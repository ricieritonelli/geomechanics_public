#ifndef SIMPLE_GEO_SIMULTANEOUS_FIELDS_UPDATER_H
#define SIMPLE_GEO_SIMULTANEOUS_FIELDS_UPDATER_H

#include "../VariableUpdater.h"

// CLASS DEFINITION ==============================================================================================
class SimpleGeoSimultaneousFieldsUpdater : public VariableUpdater
{
protected:

	// Data Members ----------------------------------------------------------------------------------------------
	VectorPtr solution;
	VectorPtr pressureField;
	VectorPtr displacementField;
	GridPtr grid;

public:

	// Constructor and Destructor --------------------------------------------------------------------------------
	SimpleGeoSimultaneousFieldsUpdater( VectorPtr solution, VectorPtr pressureField, VectorPtr displacementField, GridPtr grid );
	~SimpleGeoSimultaneousFieldsUpdater(){};

	// Class Interface -------------------------------------------------------------------------------------------
	void compute();

}; // class SimpleGeoSimultaneousFieldsUpdater

typedef SharedPointer< SimpleGeoSimultaneousFieldsUpdater > SimpleGeoSimultaneousFieldsUpdaterPtr;

#endif