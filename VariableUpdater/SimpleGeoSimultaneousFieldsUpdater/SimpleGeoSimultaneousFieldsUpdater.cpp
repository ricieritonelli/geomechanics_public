#include "SimpleGeoSimultaneousFieldsUpdater.h"

// Constructor ---------------------------------------------------------------------------------------------------
SimpleGeoSimultaneousFieldsUpdater::SimpleGeoSimultaneousFieldsUpdater( VectorPtr solution, VectorPtr pressureField, VectorPtr displacementField, GridPtr grid ) :
	solution( solution ), pressureField( pressureField ), displacementField( displacementField ), grid( grid ){}

// Class Interface -----------------------------------------------------------------------------------------------
void SimpleGeoSimultaneousFieldsUpdater::compute()
{
	int offset = ( this->grid->getCells() ).size();

	for ( int i = 0; i < ( this->grid->getCells() ).size(); ++i )
	{
		( *this->pressureField )[ i ] = ( *this->solution )[ i ];
	}

	for ( int i = 0; i < this->grid->getNumberOfFaces(); ++i )
	{
		( *this->displacementField )[ i ] = ( *this->solution )[ i + offset ];
	}
}
