#ifndef STRESS_UPDATER_H
#define STRESS_UPDATER_H

#include "../VariableUpdater.h"

// CLASS DEFINITION ==============================================================================================
class StressUpdater : public VariableUpdater
{
protected:

	// Data Members ----------------------------------------------------------------------------------------------
	VectorPtr horizontalStressField;
	VectorPtr verticalStressField;
	VectorPtr shearStressField;

	VectorPtr horizontalDeformations;
	VectorPtr verticalDeformations;
	VectorPtr crossDeformations;
	VectorPtr pressureField;
	FieldInterpolatorPtr BiotCoefficientInterpolator;
	FieldInterpolatorPtr PoissonsRatioInterpolator;
	FieldInterpolatorPtr LamesParameterInterpolator;
	FieldInterpolatorPtr shearModulusInterpolator;
	GridPtr grid;

public:

	// Constructor and Destructor --------------------------------------------------------------------------------
	StressUpdater( VectorPtr horizontalStressField, VectorPtr verticalStressField, VectorPtr shearStressField, VectorPtr horizontalDeformations, VectorPtr verticalDeformations,
		VectorPtr crossDeformations, VectorPtr pressureField, FieldInterpolatorPtr BiotCoefficientInterpolator, FieldInterpolatorPtr PoissonsRatioInterpolator,
		FieldInterpolatorPtr LamesParameterInterpolator, FieldInterpolatorPtr shearModulusInterpolator, GridPtr grid );
	~StressUpdater(){};

	// Class Interface -------------------------------------------------------------------------------------------
	void compute();

private:

	// Private Member Functions ----------------------------------------------------------------------------------
	void computeHorizontalStressField( CellPtr cell );
	void computeVerticalStressField( CellPtr cell );
	void computeShearStressField( CellPtr cell );

}; // class StressUpdater

typedef SharedPointer< StressUpdater > StressUpdaterPtr;

#endif