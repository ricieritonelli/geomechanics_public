#include "StressUpdater.h"

// Constructor ---------------------------------------------------------------------------------------------------
StressUpdater::StressUpdater( VectorPtr horizontalStressField, VectorPtr verticalStressField, VectorPtr shearStressField, VectorPtr horizontalDeformations,
	VectorPtr verticalDeformations,	VectorPtr crossDeformations, VectorPtr pressureField, FieldInterpolatorPtr BiotCoefficientInterpolator,
	FieldInterpolatorPtr PoissonsRatioInterpolator,	FieldInterpolatorPtr LamesParameterInterpolator, FieldInterpolatorPtr shearModulusInterpolator,
	GridPtr grid ) : horizontalStressField( horizontalStressField ), verticalStressField( verticalStressField ), shearStressField( shearStressField ),
	horizontalDeformations( horizontalDeformations ), verticalDeformations( verticalDeformations ), crossDeformations( crossDeformations ),
	pressureField( pressureField ), BiotCoefficientInterpolator( BiotCoefficientInterpolator ), PoissonsRatioInterpolator( PoissonsRatioInterpolator ),
	LamesParameterInterpolator( LamesParameterInterpolator ), shearModulusInterpolator( shearModulusInterpolator ), grid( grid ){}


// Class Interface -----------------------------------------------------------------------------------------------
void StressUpdater::compute()
{
	foreach( CellPtr cell, this->grid->getCells() )
	{
		this->computeHorizontalStressField( cell );
		this->computeVerticalStressField( cell );
		this->computeShearStressField( cell );
	}
}


// Private Member Functions --------------------------------------------------------------------------------------
void StressUpdater::computeHorizontalStressField( CellPtr cell )
{
	double horizontalDeformation = ( *this->horizontalDeformations )[ cell->getHandle() ];
	double verticalDeformation = ( *this->verticalDeformations )[ cell->getHandle() ];
	double pressure = ( *this->pressureField )[ cell->getHandle() ];
	
	double effectiveHorizontalStress = this->LamesParameterInterpolator->getPropertyOnCell( cell ) *
		( horizontalDeformation * ( ( 1.0 - this->PoissonsRatioInterpolator->getPropertyOnCell( cell ) ) / this->PoissonsRatioInterpolator->getPropertyOnCell( cell ) ) + verticalDeformation );

	double horizontalStress = effectiveHorizontalStress - this->BiotCoefficientInterpolator->getPropertyOnCell( cell ) * pressure;
	( *this->horizontalStressField )[ cell->getHandle() ] = horizontalStress;
}


void StressUpdater::computeVerticalStressField( CellPtr cell )
{
	double horizontalDeformation = ( *this->horizontalDeformations )[ cell->getHandle() ];
	double verticalDeformation = ( *this->verticalDeformations )[ cell->getHandle() ];
	double pressure = ( *this->pressureField )[ cell->getHandle() ];

	double effectiveVerticalStress = this->LamesParameterInterpolator->getPropertyOnCell( cell ) *
		( horizontalDeformation + verticalDeformation * ( ( 1.0 - this->PoissonsRatioInterpolator->getPropertyOnCell( cell ) ) / this->PoissonsRatioInterpolator->getPropertyOnCell( cell ) ) );

	double verticalStress = effectiveVerticalStress - this->BiotCoefficientInterpolator->getPropertyOnCell( cell ) * pressure;
	( *this->verticalStressField )[ cell->getHandle() ] = verticalStress;
}


void StressUpdater::computeShearStressField( CellPtr cell )
{
	double crossDeformation = ( *this->crossDeformations )[ cell->getHandle() ];

	double shearStress = 2 * this->shearModulusInterpolator->getPropertyOnCell( cell ) * crossDeformation;
	( *this->shearStressField )[ cell->getHandle() ] = shearStress;
}