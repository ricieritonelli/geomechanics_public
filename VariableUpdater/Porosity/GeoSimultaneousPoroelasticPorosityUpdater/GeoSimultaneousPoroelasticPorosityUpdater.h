#ifndef GEO_SIMULTANEOUS_POROELASTIC_POROSITY_UPDATER_H
#define GEO_SIMULTANEOUS_POROELASTIC_POROSITY_UPDATER_H

#include "../../VariableUpdater.h"

// CLASS DEFINITION ==============================================================================================
class GeoSimultaneousPoroelasticPorosityUpdater : public VariableUpdater
{
protected:

	// Data Members ----------------------------------------------------------------------------------------------
	VectorPtr porosity;
	VectorPtr porosityOld;
	VectorPtr solution;
	VectorPtr solutionOld;
	FieldInterpolatorPtr BiotCoefficientInterpolator;
	FieldInterpolatorPtr solidPhaseCompressibilityInterpolator;
	GridPtr grid;

public:

	// Constructor and Destructor --------------------------------------------------------------------------------
	GeoSimultaneousPoroelasticPorosityUpdater( VectorPtr porosity, VectorPtr porosityOld, VectorPtr solution, VectorPtr solutionOld, FieldInterpolatorPtr BiotCoefficientInterpolator,
		FieldInterpolatorPtr solidPhaseCompressibilityInterpolator, GridPtr grid);
	~GeoSimultaneousPoroelasticPorosityUpdater(){};

	// Class Interface -------------------------------------------------------------------------------------------
	void compute();

}; // class GeoSimultaneousPoroelasticPorosityUpdater

typedef SharedPointer< GeoSimultaneousPoroelasticPorosityUpdater > GeoSimultaneousPoroelasticPorosityUpdaterPtr;

#endif