#include "GeoSimultaneousPoroelasticPorosityUpdater.h"

// Constructor ---------------------------------------------------------------------------------------------------
GeoSimultaneousPoroelasticPorosityUpdater::GeoSimultaneousPoroelasticPorosityUpdater( VectorPtr porosity, VectorPtr porosityOld, VectorPtr solution, VectorPtr solutionOld,
	FieldInterpolatorPtr BiotCoefficientInterpolator, FieldInterpolatorPtr solidPhaseCompressibilityInterpolator, GridPtr grid) : porosity( porosity ), porosityOld( porosityOld ),
	solution( solution ), solutionOld( solutionOld ), BiotCoefficientInterpolator( BiotCoefficientInterpolator ), solidPhaseCompressibilityInterpolator( solidPhaseCompressibilityInterpolator ),
	grid( grid ){}


// Class Interface -----------------------------------------------------------------------------------------------
void GeoSimultaneousPoroelasticPorosityUpdater::compute()
{
	int offset = ( this->grid->getCells() ).size();

	foreach( CellPtr cell, this->grid->getCells() )
	{
		FacePtr faceWest = cell->getWestFaceID();
		FacePtr faceEast = cell->getEastFaceID();
		FacePtr faceSouth = cell->getSouthFaceID();
		FacePtr faceNorth = cell->getNorthFaceID();

		double dx = faceEast->getCentroid()->dist( *faceWest->getCentroid() );
		double dy = faceNorth->getCentroid()->dist( *faceSouth->getCentroid() );

		double horizontalDeformation = ( ( *this->solution )[ faceEast->getHandle() + offset ] - ( *this->solution )[ faceWest->getHandle() + offset ] ) / dx;
		double verticalDeformation = ( ( *this->solution )[ faceNorth->getHandle() + offset ] - ( *this->solution )[ faceSouth->getHandle() + offset ] ) / dy;
		double deformation = horizontalDeformation + verticalDeformation;

		double horizontalDeformationOld = ( ( *this->solutionOld )[ faceEast->getHandle() + offset ] - ( *this->solutionOld )[ faceWest->getHandle() + offset ] ) / dx;
		double verticalDeformationOld = ( ( *this->solutionOld )[ faceNorth->getHandle() + offset ] - ( *this->solutionOld )[ faceSouth->getHandle() + offset ] ) / dy;
		double deformationOld = horizontalDeformationOld + verticalDeformationOld;

		( *this->porosity )[ cell->getHandle() ] = ( *this->porosityOld )[ cell->getHandle() ] +
			( this->BiotCoefficientInterpolator->getPropertyOnCell( cell ) - ( *this->porosityOld )[ cell->getHandle() ] ) * ( deformation - deformationOld ) +
			this->solidPhaseCompressibilityInterpolator->getPropertyOnCell( cell ) * ( this->BiotCoefficientInterpolator->getPropertyOnCell( cell ) - ( *this->porosityOld )[ cell->getHandle() ] ) *
			( ( *this->solution )[cell->getHandle()] - ( *this->solutionOld )[cell->getHandle()] );

// 		cout << "dx = " << dx << endl;
// 		cout << "dy = " << dy << endl;
// 		cout << "Horizontal Deformation = " << horizontalDeformation << endl;
// 		cout << "Vertical Deformation = " << verticalDeformation << endl;
// 		cout << "Deformation = " << deformation << endl;
// 		cout << "Horizontal Deformation Old = " << horizontalDeformationOld << endl;
// 		cout << "Vertical Deformation Old = " << verticalDeformationOld << endl;
// 		cout << "Deformation Old = " << deformationOld << endl << endl;
	}
}

