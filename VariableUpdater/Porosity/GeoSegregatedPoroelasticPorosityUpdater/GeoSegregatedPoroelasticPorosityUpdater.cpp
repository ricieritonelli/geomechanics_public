#include "GeoSegregatedPoroelasticPorosityUpdater.h"

// Constructor ---------------------------------------------------------------------------------------------------
GeoSegregatedPoroelasticPorosityUpdater::GeoSegregatedPoroelasticPorosityUpdater( VectorPtr porosity, VectorPtr porosityOld, VectorPtr displacementField, VectorPtr displacementFieldOld, VectorPtr pressureField,
	VectorPtr pressureFieldOld, FieldInterpolatorPtr BiotCoefficientInterpolator, FieldInterpolatorPtr solidPhaseCompressibilityInterpolator, GridPtr grid) : porosity( porosity ),
	porosityOld( porosityOld ),	displacementField( displacementField ), displacementFieldOld( displacementFieldOld ), pressureField( pressureField ), pressureFieldOld( pressureFieldOld ),
	BiotCoefficientInterpolator( BiotCoefficientInterpolator ), solidPhaseCompressibilityInterpolator( solidPhaseCompressibilityInterpolator ), grid( grid ){}


// Class Interface -----------------------------------------------------------------------------------------------
void GeoSegregatedPoroelasticPorosityUpdater::compute()
{
	foreach( CellPtr cell, this->grid->getCells() )
	{
		FacePtr faceWest = cell->getWestFaceID();
		FacePtr faceEast = cell->getEastFaceID();
		FacePtr faceSouth = cell->getSouthFaceID();
		FacePtr faceNorth = cell->getNorthFaceID();

		double dx = faceEast->getCentroid()->dist( *faceWest->getCentroid() );
		double dy = faceNorth->getCentroid()->dist( *faceSouth->getCentroid() );

		double horizontalDeformation = ( ( *this->displacementField )[ faceEast->getHandle() ] - ( *this->displacementField )[ faceWest->getHandle() ] ) / dx;
		double verticalDeformation = ( ( *this->displacementField )[ faceNorth->getHandle() ] - ( *this->displacementField )[ faceSouth->getHandle() ] ) / dy;
		double deformation = horizontalDeformation + verticalDeformation;

		double horizontalDeformationOld = ( ( *this->displacementFieldOld )[ faceEast->getHandle() ] - ( *this->displacementFieldOld )[ faceWest->getHandle() ] ) / dx;
		double verticalDeformationOld = ( ( *this->displacementFieldOld )[ faceNorth->getHandle() ] - ( *this->displacementFieldOld )[ faceSouth->getHandle() ] ) / dy;
		double deformationOld = horizontalDeformationOld + verticalDeformationOld;

		( *this->porosity )[ cell->getHandle() ] = ( *this->porosityOld )[ cell->getHandle() ] +
			( this->BiotCoefficientInterpolator->getPropertyOnCell( cell ) - ( *this->porosityOld )[ cell->getHandle() ] ) * ( deformation - deformationOld ) +
			this->solidPhaseCompressibilityInterpolator->getPropertyOnCell( cell ) * ( this->BiotCoefficientInterpolator->getPropertyOnCell( cell ) - ( *this->porosityOld )[ cell->getHandle() ] ) *
			( ( *this->pressureField )[cell->getHandle()] - ( *this->pressureFieldOld )[cell->getHandle()] );

// 		cout << "dx = " << dx << endl;
// 		cout << "dy = " << dy << endl;
// 		cout << "Horizontal Deformation = " << horizontalDeformation << endl;
// 		cout << "Vertical Deformation = " << verticalDeformation << endl;
// 		cout << "Deformation = " << deformation << endl;
// 		cout << "Horizontal Deformation Old = " << horizontalDeformationOld << endl;
// 		cout << "Vertical Deformation Old = " << verticalDeformationOld << endl;
// 		cout << "Deformation Old = " << deformationOld << endl;
	}
}
