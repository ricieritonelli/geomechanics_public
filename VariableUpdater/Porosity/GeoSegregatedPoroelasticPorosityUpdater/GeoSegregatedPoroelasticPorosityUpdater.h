#ifndef GEO_SEGREGATED_POROELASTIC_POROSITY_UPDATER_H
#define GEO_SEGREGATED_POROELASTIC_POROSITY_UPDATER_H

#include "../../VariableUpdater.h"

// CLASS DEFINITION ==============================================================================================
class GeoSegregatedPoroelasticPorosityUpdater : public VariableUpdater
{
protected:

	// Data Members ----------------------------------------------------------------------------------------------
	VectorPtr porosity;
	VectorPtr porosityOld;
	VectorPtr displacementField;
	VectorPtr displacementFieldOld;
	VectorPtr pressureField;
	VectorPtr pressureFieldOld;
	FieldInterpolatorPtr BiotCoefficientInterpolator;
	FieldInterpolatorPtr solidPhaseCompressibilityInterpolator;
	GridPtr grid;

public:

	// Constructor and Destructor --------------------------------------------------------------------------------
	GeoSegregatedPoroelasticPorosityUpdater( VectorPtr porosity, VectorPtr porosityOld, VectorPtr displacementField, VectorPtr displacementFieldOld, VectorPtr pressureField,
		VectorPtr pressureFieldOld, FieldInterpolatorPtr BiotCoefficientInterpolator, FieldInterpolatorPtr solidPhaseCompressibilityInterpolator, GridPtr grid);
	~GeoSegregatedPoroelasticPorosityUpdater(){};

	// Class Interface -------------------------------------------------------------------------------------------
	void compute();

}; // class GeoSegregatedPoroelasticPorosityUpdater

typedef SharedPointer< GeoSegregatedPoroelasticPorosityUpdater > GeoSegregatedPoroelasticPorosityUpdaterPtr;

#endif