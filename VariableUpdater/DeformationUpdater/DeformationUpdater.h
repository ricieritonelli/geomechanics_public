#ifndef DEFORMATION_UPDATER_H
#define DEFORMATION_UPDATER_H

#include "../VariableUpdater.h"

// CLASS DEFINITION ==============================================================================================
class DeformationUpdater : public VariableUpdater
{
protected:

	// Data Members ----------------------------------------------------------------------------------------------
	VectorPtr horizontalDeformationField;
	VectorPtr verticalDeformationField;
	VectorPtr crossDeformationField;
	VectorPtr displacementField;
	GridPtr grid;

public:

	// Constructor and Destructor --------------------------------------------------------------------------------
	DeformationUpdater( VectorPtr horizontalDeformationField, VectorPtr verticalDeformationField, VectorPtr crossDeformationField,
		VectorPtr displacementField, GridPtr grid);
	~DeformationUpdater(){};

	// Class Interface -------------------------------------------------------------------------------------------
	void compute();

private:

	// Private Member Functions ----------------------------------------------------------------------------------
	void computeHorizontalDeformation( CellPtr cell );
	void computeVerticalDeformation( CellPtr cell );
	void computeCrossDeformation( CellPtr cell );

	double computeHorizontalDisplacementSouthFace( CellPtr cell );
	double computeHorizontalDisplacementNorthFace( CellPtr cell );
	double computeVerticalDisplacementWestFace( CellPtr cell );
	double computeVerticalDisplacementEastFace( CellPtr cell );

	double extrapolateBackwards( VertexPtr vertexBack, FacePtr faceBack, FacePtr faceFront );
	double extrapolateFrontwards( FacePtr faceBack, FacePtr faceFront, VertexPtr vertexFront );
	double interpolateUsingFourValuesEquallySpaced( FacePtr face1, FacePtr face2, FacePtr face3, FacePtr face4 );

}; // class DeformationUpdater

typedef SharedPointer< DeformationUpdater > DeformationUpdaterPtr;

#endif