#include "DeformationUpdater.h"

// Constructor ---------------------------------------------------------------------------------------------------
DeformationUpdater::DeformationUpdater( VectorPtr horizontalDeformationField, VectorPtr verticalDeformationField, VectorPtr crossDeformationField,
	VectorPtr displacementField, GridPtr grid ) : horizontalDeformationField( horizontalDeformationField ), verticalDeformationField( verticalDeformationField ),
	crossDeformationField( crossDeformationField ), displacementField( displacementField ), grid( grid ){}


// Class Interface -----------------------------------------------------------------------------------------------
void DeformationUpdater::compute()
{
	foreach( CellPtr cell, this->grid->getCells() )
	{
		this->computeHorizontalDeformation( cell );
		this->computeVerticalDeformation( cell );
		this->computeCrossDeformation( cell );
	}
}


// Private Member Functions --------------------------------------------------------------------------------------
void DeformationUpdater::computeHorizontalDeformation( CellPtr cell )
{
	FacePtr faceWest = cell->getWestFaceID();
	FacePtr faceEast = cell->getEastFaceID();
	double dx = faceEast->getCentroid()->dist( *faceWest->getCentroid() );

	double horizontalDeformation = ( ( *this->displacementField )[ faceEast->getHandle() ] - ( *this->displacementField )[ faceWest->getHandle() ] ) / dx;
	( *this->horizontalDeformationField )[ cell->getHandle() ] = horizontalDeformation;
}


void DeformationUpdater::computeVerticalDeformation( CellPtr cell )
{
	FacePtr faceSouth = cell->getSouthFaceID();
	FacePtr faceNorth = cell->getNorthFaceID();
	double dy = faceNorth->getCentroid()->dist( *faceSouth->getCentroid() );

	double verticalDeformation = ( ( *this->displacementField )[ faceNorth->getHandle() ] - ( *this->displacementField )[ faceSouth->getHandle() ] ) / dy;
	( *this->verticalDeformationField )[ cell->getHandle() ] = verticalDeformation;
}


void DeformationUpdater::computeCrossDeformation( CellPtr cell )
{
	double dx = cell->getEastFaceID()->getCentroid()->dist( *cell->getWestFaceID()->getCentroid() );
	double dy = cell->getNorthFaceID()->getCentroid()->dist( *cell->getSouthFaceID()->getCentroid() );

	double horizontalDisplacementSouth = this->computeHorizontalDisplacementSouthFace( cell );
	double horizontalDisplacementNorth = this->computeHorizontalDisplacementNorthFace( cell );
	double verticalDisplacementWest = this->computeVerticalDisplacementWestFace( cell );
	double verticalDisplacementEast = this->computeVerticalDisplacementEastFace( cell );

	double crossDeformation = ( ( horizontalDisplacementNorth - horizontalDisplacementSouth ) / dy  + ( verticalDisplacementEast - verticalDisplacementWest ) / dx ) / 2;
	( *this->crossDeformationField )[ cell->getHandle() ] = crossDeformation;
}


double DeformationUpdater::computeHorizontalDisplacementSouthFace( CellPtr cell )
{
	FacePtr faceWest = cell->getWestFaceID();
	FacePtr faceEast = cell->getEastFaceID();

	if ( cell->getHandle() < this->grid->getNx() ) /* South boundary */
	{
		InternalFacePtr faceNorth = ( this->grid->getInternalFaces() )[ cell->getNorthFaceID()->getHandle() ];
		CellPtr cellNorth = ( this->grid->getCells() )[ faceNorth->getNode1()->getHandle() ];
		FacePtr faceNorthWest = cellNorth->getWestFaceID();
		FacePtr faceNorthEast = cellNorth->getEastFaceID();
		VertexPtr vertexSouthWest = ( faceWest->getVertices() )[ 0 ];
		VertexPtr vertexSouthEast = ( faceEast->getVertices() )[ 0 ];

		double horizontalDisplacementSouthWest = this->extrapolateBackwards( vertexSouthWest, faceWest, faceNorthWest );
		double horizontalDisplacementSouthEast = this->extrapolateBackwards( vertexSouthEast, faceEast, faceNorthEast );

		double horizontalDisplacementSouthFace = ( horizontalDisplacementSouthWest + horizontalDisplacementSouthEast ) / 2;

		return horizontalDisplacementSouthFace;
	} 
	else
	{
		InternalFacePtr faceSouth = ( this->grid->getInternalFaces() )[ cell->getSouthFaceID()->getHandle() ];
		CellPtr cellSouth = ( this->grid->getCells() )[ faceSouth->getNode0()->getHandle() ];
		FacePtr faceSouthWest = cellSouth->getWestFaceID();
		FacePtr faceSouthEast = cellSouth->getEastFaceID();

		double horizontalDisplacementSouthFace = this->interpolateUsingFourValuesEquallySpaced( faceWest, faceEast, faceSouthWest, faceSouthEast );

		return horizontalDisplacementSouthFace;
	}
}


double DeformationUpdater::computeHorizontalDisplacementNorthFace( CellPtr cell )
{
	FacePtr faceWest = cell->getWestFaceID();
	FacePtr faceEast = cell->getEastFaceID();

	if ( cell->getHandle() >= this->grid->getNx() * ( this->grid->getNy() - 1 ) ) /* North boundary */
	{
		InternalFacePtr faceSouth = ( this->grid->getInternalFaces() )[ cell->getSouthFaceID()->getHandle() ];
		CellPtr cellSouth = ( this->grid->getCells() )[ faceSouth->getNode0()->getHandle() ];
		FacePtr faceSouthWest = cellSouth->getWestFaceID();
		FacePtr faceSouthEast = cellSouth->getEastFaceID();
		VertexPtr vertexNorthWest = ( faceWest->getVertices() )[ 1 ];
		VertexPtr vertexNorthEast = ( faceEast->getVertices() )[ 1 ];

		double horizontalDisplacementNorthWest = this->extrapolateFrontwards( faceSouthWest, faceWest, vertexNorthWest );
		double horizontalDisplacementNorthEast = this->extrapolateFrontwards( faceSouthEast, faceEast, vertexNorthEast );

		double horizontalDisplacementNorthFace = ( horizontalDisplacementNorthWest + horizontalDisplacementNorthEast ) / 2;

		return horizontalDisplacementNorthFace;
	} 
	else
	{
		InternalFacePtr faceNorth = ( this->grid->getInternalFaces() )[ cell->getNorthFaceID()->getHandle() ];
		CellPtr cellNorth = ( this->grid->getCells() )[ faceNorth->getNode1()->getHandle() ];
		FacePtr faceNorthWest = cellNorth->getWestFaceID();
		FacePtr faceNorthEast = cellNorth->getEastFaceID();

		double horizontalDisplacementNorthFace = this->interpolateUsingFourValuesEquallySpaced( faceWest, faceEast, faceNorthWest, faceNorthEast );

		return horizontalDisplacementNorthFace;
	}
}


double DeformationUpdater::computeVerticalDisplacementWestFace( CellPtr cell )
{
	FacePtr faceSouth = cell->getSouthFaceID();
	FacePtr faceNorth = cell->getNorthFaceID();

	if ( cell->getHandle() % this->grid->getNx() == 0 ) /* West boundary */
	{
		InternalFacePtr faceEast = ( this->grid->getInternalFaces() )[ cell->getEastFaceID()->getHandle() ];
		CellPtr cellEast = ( this->grid->getCells() )[ faceEast->getNode1()->getHandle() ];
		FacePtr faceEastSouth = cellEast->getSouthFaceID();
		FacePtr faceEastNorth = cellEast->getNorthFaceID();
		VertexPtr vertexWestSouth = ( faceSouth->getVertices() )[ 0 ];
		VertexPtr vertexWestNorth = ( faceNorth->getVertices() )[ 0 ];

		double verticalDisplacementWestSouth = this->extrapolateBackwards( vertexWestSouth, faceSouth, faceEastSouth );
		double verticalDisplacementWestNorth = this->extrapolateBackwards( vertexWestNorth, faceNorth, faceEastNorth );

		double verticalDisplacementWestFace = ( verticalDisplacementWestSouth + verticalDisplacementWestNorth ) / 2;

		return verticalDisplacementWestFace;
	} 
	else
	{
		InternalFacePtr faceWest = ( this->grid->getInternalFaces() )[ cell->getWestFaceID()->getHandle() ];
		CellPtr cellWest = ( this->grid->getCells() )[ faceWest->getNode0()->getHandle() ];
		FacePtr faceWestSouth = cellWest->getSouthFaceID();
		FacePtr faceWestNorth = cellWest->getNorthFaceID();

		double verticallDisplacementWestFace = this->interpolateUsingFourValuesEquallySpaced( faceWestSouth, faceSouth, faceWestNorth, faceNorth );

		return verticallDisplacementWestFace;
	}
}


double DeformationUpdater::computeVerticalDisplacementEastFace( CellPtr cell )
{
	FacePtr faceSouth = cell->getSouthFaceID();
	FacePtr faceNorth = cell->getNorthFaceID();

	if ( ( cell->getHandle() + 1 ) % this->grid->getNx() == 0 ) /* East boundary */
	{
		InternalFacePtr faceWest = ( this->grid->getInternalFaces() )[ cell->getWestFaceID()->getHandle() ];
		CellPtr cellWest = ( this->grid->getCells() )[ faceWest->getNode0()->getHandle() ];
		FacePtr faceWestSouth = cellWest->getSouthFaceID();
		FacePtr faceWestNorth = cellWest->getNorthFaceID();
		VertexPtr vertexEastSouth = ( faceSouth->getVertices() )[ 1 ];
		VertexPtr vertexEastNorth = ( faceNorth->getVertices() )[ 1 ];

		double verticalDisplacementEastSouth = this->extrapolateFrontwards( faceWestSouth, faceSouth, vertexEastSouth );
		double verticalDisplacementEastNorth = this->extrapolateFrontwards( faceWestNorth, faceNorth, vertexEastNorth );

		double verticalDisplacementEastFace = ( verticalDisplacementEastSouth + verticalDisplacementEastNorth ) / 2;

		return verticalDisplacementEastFace;
	}
	else
	{
		InternalFacePtr faceEast = ( this->grid->getInternalFaces() )[ cell->getEastFaceID()->getHandle() ];
		CellPtr cellEast = ( this->grid->getCells() )[ faceEast->getNode1()->getHandle() ];
		FacePtr faceEastSouth = cellEast->getSouthFaceID();
		FacePtr faceEastNorth = cellEast->getNorthFaceID();

		double verticalDisplacementEastFace = this->interpolateUsingFourValuesEquallySpaced( faceSouth, faceEastSouth, faceNorth, faceEastNorth );

		return verticalDisplacementEastFace;
	}
}


double DeformationUpdater::extrapolateBackwards( VertexPtr vertexBack, FacePtr faceBack, FacePtr faceFront )
{
	double displacementBack = ( *this->displacementField )[ faceBack->getHandle() ];
	double displacementFront = ( *this->displacementField )[ faceFront->getHandle() ];

	double backwardsExtrapolatedDisplacement = displacementBack - faceBack->getCentroid()->dist( *vertexBack ) *
		( ( displacementFront - displacementBack ) / faceFront->getCentroid()->dist( *faceBack->getCentroid() ) );

	return backwardsExtrapolatedDisplacement;
}


double DeformationUpdater::extrapolateFrontwards( FacePtr faceBack, FacePtr faceFront, VertexPtr vertexFront )
{
	double displacementBack = ( *this->displacementField )[ faceBack->getHandle() ];
	double displacementFront = ( *this->displacementField )[ faceFront->getHandle() ];

	double frontwardsExtrapolatedDisplacement = displacementFront + vertexFront->dist( *faceFront->getCentroid() ) *
		( ( displacementFront - displacementBack ) / faceFront->getCentroid()->dist( *faceBack->getCentroid() ) );

	return frontwardsExtrapolatedDisplacement;
}


double DeformationUpdater::interpolateUsingFourValuesEquallySpaced( FacePtr face1, FacePtr face2, FacePtr face3, FacePtr face4 )
{
	double displacement1 = ( *this->displacementField )[ face1->getHandle() ];
	double displacement2 = ( *this->displacementField )[ face2->getHandle() ];
	double displacement3 = ( *this->displacementField )[ face3->getHandle() ];
	double displacement4 = ( *this->displacementField )[ face4->getHandle() ];

	return ( displacement1 + displacement2 + displacement3 + displacement4 ) / 4;
}