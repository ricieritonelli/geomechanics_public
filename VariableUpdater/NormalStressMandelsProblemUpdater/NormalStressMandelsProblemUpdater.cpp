#include "NormalStressMandelsProblemUpdater.h"

// Constructor ---------------------------------------------------------------------------------------------------
NormalStressMandelsProblemUpdater::NormalStressMandelsProblemUpdater( const double& force, double& normalStress, VectorPtr horizontalStressField,
	VectorPtr verticalStressField, BoundaryPtr boundary ) : force( force ), normalStress( normalStress ), horizontalStressField( horizontalStressField ),
	verticalStressField( verticalStressField ), boundary( boundary ){}


// Class Interface -----------------------------------------------------------------------------------------------
void NormalStressMandelsProblemUpdater::compute()
{
	double totalArea = 0;
	double totalForce = 0;

	if ( abs( ( ( this->boundary->getFaces() )[0] )->getAreaVector()->x() ) > abs( ( ( this->boundary->getFaces() )[0] )->getAreaVector()->y() ) ) /* WEST, EAST */
	{
		foreach( ExternalFacePtr face, this->boundary->getFaces() )
		{
			totalArea += face->getArea();
			totalForce += face->getArea() * ( *this->horizontalStressField )[ face->getTopologyNode()->getHandle() ];
		}
	} 
	else /* SOUTH, NORTH */
	{
		foreach( ExternalFacePtr face, this->boundary->getFaces() )
		{
			totalArea += face->getArea();
			totalForce += face->getArea() * ( *this->verticalStressField )[ face->getTopologyNode()->getHandle() ];
		}
	}

	this->normalStress = this->normalStress - ( totalForce - this->force ) / totalArea;
}
