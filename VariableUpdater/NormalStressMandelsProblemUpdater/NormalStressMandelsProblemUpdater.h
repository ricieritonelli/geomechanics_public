#ifndef NORMAL_STRESS_MANDELS_PROBLEM_UPDATER_H
#define NORMAL_STRESS_MANDELS_PROBLEM_UPDATER_H

#include "../VariableUpdater.h"

// CLASS DEFINITION ==============================================================================================
class NormalStressMandelsProblemUpdater : public VariableUpdater
{
protected:

	// Data Members ----------------------------------------------------------------------------------------------
	const double& force;
	double& normalStress;
	VectorPtr horizontalStressField;
	VectorPtr verticalStressField;
	BoundaryPtr boundary;

public:

	// Constructor and Destructor --------------------------------------------------------------------------------
	NormalStressMandelsProblemUpdater( const double& force, double& normalStress, VectorPtr horizontalStressField,
		VectorPtr verticalStressField, BoundaryPtr boundary );
	~NormalStressMandelsProblemUpdater(){};

	// Class Interface -------------------------------------------------------------------------------------------
	void compute();

}; // class NormalStressMandelsProblemUpdater

typedef SharedPointer< NormalStressMandelsProblemUpdater > NormalStressMandelsProblemUpdaterPtr;

#endif