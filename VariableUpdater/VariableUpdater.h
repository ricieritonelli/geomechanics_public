#ifndef VARIABLE_UPDATER_H
#define VARIABLE_UPDATER_H

#include "../Config.h"
#include "../VariableComputer/VariableComputer.h"

class VariableUpdater : public VariableComputer
{
public:
	VariableUpdater(){}

	virtual void compute() = 0;

	virtual ~VariableUpdater(){}

}; //class VariableUpdater

typedef SharedPointer< VariableUpdater > VariableUpdaterPtr;

#endif