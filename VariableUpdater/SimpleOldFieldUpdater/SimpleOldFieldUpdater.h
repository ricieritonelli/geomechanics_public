#ifndef SIMPLE_OLD_FIELD_UPDATER_H
#define SIMPLE_OLD_FIELD_UPDATER_H

#include "../VariableUpdater.h"

// CLASS DEFINITION ==============================================================================================
class SimpleOldFieldUpdater : public VariableUpdater
{
protected:

	// Data Members ----------------------------------------------------------------------------------------------
	VectorPtr field;
	VectorPtr oldField;

public:

	// Constructor and Destructor --------------------------------------------------------------------------------
	SimpleOldFieldUpdater( VectorPtr field, VectorPtr oldField );
	~SimpleOldFieldUpdater(){};

	// Class Interface -------------------------------------------------------------------------------------------
	void compute();

}; // class SimpleOldFieldUpdater

typedef SharedPointer< SimpleOldFieldUpdater > SimpleOldFieldUpdaterPtr;

#endif