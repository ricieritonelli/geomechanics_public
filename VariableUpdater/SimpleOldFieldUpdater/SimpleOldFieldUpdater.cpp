#include "SimpleOldFieldUpdater.h"

// Constructor ---------------------------------------------------------------------------------------------------
SimpleOldFieldUpdater::SimpleOldFieldUpdater( VectorPtr field, VectorPtr oldField ) : field( field ), oldField( oldField ){}

// Class Interface -----------------------------------------------------------------------------------------------
void SimpleOldFieldUpdater::compute()
{
	*(this->oldField) = *(this->field);
}
