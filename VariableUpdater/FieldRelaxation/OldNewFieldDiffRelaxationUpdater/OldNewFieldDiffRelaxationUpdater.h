#ifndef OLD_NEW_FIELD_DIFF_RELAXATION_UPDATER_H
#define OLD_NEW_FIELD_DIFF_RELAXATION_UPDATER_H

#include "../../VariableUpdater.h"

// CLASS DEFINITION ==============================================================================================
class OldNewFieldDiffRelaxationUpdater : public VariableUpdater
{
protected:

	// Data Members ----------------------------------------------------------------------------------------------
	VectorPtr field;
	VectorPtr oldField;
	double relaxationCoeff;

public:

	// Constructor and Destructor --------------------------------------------------------------------------------
	OldNewFieldDiffRelaxationUpdater( VectorPtr field, VectorPtr oldField, const double& relaxationCoeff );
	~OldNewFieldDiffRelaxationUpdater(){};

	// Class Interface -------------------------------------------------------------------------------------------
	void compute();

}; // class OldNewFieldDiffRelaxationUpdater

typedef SharedPointer< OldNewFieldDiffRelaxationUpdater > OldNewFieldDiffRelaxationUpdaterPtr;

#endif