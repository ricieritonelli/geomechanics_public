#include "OldNewFieldDiffRelaxationUpdater.h"

// Constructor ---------------------------------------------------------------------------------------------------
OldNewFieldDiffRelaxationUpdater::OldNewFieldDiffRelaxationUpdater( VectorPtr field, VectorPtr oldField, const double& relaxationCoeff ) : field( field ),
	oldField( oldField ), relaxationCoeff( relaxationCoeff ){}

// Class Interface -----------------------------------------------------------------------------------------------
void OldNewFieldDiffRelaxationUpdater::compute()
{
	for (int i = 0; i != this->field->size(); ++i)
	{
		( *this->field )[ i ] = ( *this->oldField )[ i ] + this->relaxationCoeff * ( ( *this->field )[ i ] - ( *this->oldField )[ i ] );
	}
}
